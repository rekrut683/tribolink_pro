package pro.tribo.TriboPro;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pro.tribo.TriboPro.R;

public class ScannerActivity extends AppCompatActivity implements View.OnClickListener {

    private ScannerActivity.LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private BluetoothLeScanner bluetoothLeScanner;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 10000;

    private RecyclerView rv;
    private List<RecyclerItem> listItems;
    private ArrayList<BluetoothDevice> listLeDevices;
    private ViewLeAdapter vLeAdapter;

    private Handler mHandler;

    private LinearLayout mSearchLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.search1) {
            //Toast.makeText(this, "Start scan", Toast.LENGTH_SHORT).show();
            mSearchLayout.setVisibility(View.VISIBLE);
            scanLeDevice(false);
        } else if(v.getId() == R.id.ret1) {
            stopScan();
            finish();
        }  else {
            //Toast.makeText(this, "another", Toast.LENGTH_SHORT).show();
        }
    }

    public class RecyclerItem {
        private String devMacView;
        private String mac;

        public RecyclerItem(String macView, String mac) {
            this.devMacView = macView;
            this.mac = mac;
        }

        public String getMacView() {
            return devMacView;
        }

        public void setMac(String mac) {
            this.devMacView = mac;
            this.mac = mac;
        }
    }

    public class ViewLeAdapter extends RecyclerView.Adapter<ScannerActivity.ViewLeAdapter.ViewHolder> {
        private List<ScannerActivity.RecyclerItem> listItems;
        private Context mContext;
        public ViewLeAdapter(Context mContext) {
            this.listItems = new ArrayList<>();
            this.mContext = mContext;
        }

        @Override
        public ScannerActivity.ViewLeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.elem_view_scan, parent, false);
            return new ScannerActivity.ViewLeAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ScannerActivity.ViewLeAdapter.ViewHolder holder, final int position) {
            final ScannerActivity.RecyclerItem itemList = listItems.get(position);
            holder.txtMac.setText(itemList.getMacView());
            holder.mac = itemList.mac;
            holder.txtMac.setOnClickListener(v -> {
                PopupMenu menu = new PopupMenu(mContext, holder.txtMac);
                menu.inflate(R.menu.scan_menu);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            // нажатие кнопки "Добавить"
                            case R.id.menu2_1:
                                //Получаем вид с файла prompt.xml, который применим для диалогового окна:
                                LayoutInflater li = LayoutInflater.from(mContext);
                                View promptsView = li.inflate(R.layout.prompt_main, null);
                                //Создаем AlertDialog
                                //AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(mContext);
                                //Настраиваем prompt.xml для нашего AlertDialog:
                                //mDialogBuilder.setView(promptsView);
                                //Настраиваем отображение поля для ввода текста в открытом диалоге:
                                final EditText userInput = (EditText) promptsView.findViewById(R.id.input_text);
                                final AlertDialog dialog = new AlertDialog.Builder(mContext)
                                        .setView(promptsView)
                                        .setPositiveButton("OK", null) //Set to null. We override the onclick
                                        .setNegativeButton("Отмена", null)
                                        .create();

                                dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialogInterface) {

                                        Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                                        button.setOnClickListener(new View.OnClickListener() {

                                            @Override
                                            public void onClick(View view) {
                                                // подключимся к БД
                                                SQLiteDatabase db = getBaseContext().openOrCreateDatabase("appTribo.db", MODE_PRIVATE, null);
                                                // берем курсор базы
                                                Cursor query = db.rawQuery("SELECT name,mac FROM devices1;", null);
                                                // берем введенную строку имени
                                                String newName = userInput.getText().toString();
                                                // флаг присутствия данного имени
                                                boolean isFindName = false;
                                                boolean isFindMac = false;
                                                //Log.d("fgdg","MACCC: "+holder.mac);
                                                // делаем запрос к базе
                                                while (query.moveToNext()) {
                                                    if (query.getString(0) == newName) {
                                                        isFindName = true;
                                                    }
                                                    if(query.getString(1).equals(holder.mac))
                                                    {
                                                        isFindMac = true;
                                                    }
                                                }
                                                query.close();
                                                if(isFindName || newName.isEmpty())
                                                {
                                                    TextView tv1 = (TextView)dialog.findViewById(R.id.tvError);
                                                    tv1.setVisibility(View.VISIBLE);
                                                    tv1.setText("Данное имя некорректно");
                                                }
                                                else if(isFindMac)
                                                {
                                                    TextView tv1 = (TextView)dialog.findViewById(R.id.tvError);
                                                    tv1.setVisibility(View.VISIBLE);
                                                    tv1.setText("Устройство уже есть в БД");
                                                }
                                                else
                                                {
                                                    //String nname = newName;
                                                    Log.d("DFdfd","name: "+newName+", mac: "+holder.mac);
                                                    db.execSQL("INSERT INTO devices1 (name,mac) VALUES('"+newName+"','"+holder.mac+"')");
                                                    db.close();
                                                    dialog.dismiss();


                                                }
                                                //query.close();
                                                //dialog.dismiss();
                                            }
                                        });
                                    }

                                });
                                dialog.show();





                                //Настраиваем сообщение в диалоговом окне:
                                /*mDialogBuilder
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,int id) {
                                                            // берем курсор базы
                                                            Cursor query = db.rawQuery("SELECT name FROM devices1;", null);
                                                            // берем введенную строку имени
                                                            String newName = userInput.getText().toString();
                                                            Log.d("GGGG", userInput.getText().toString());
                                                            // флаг присутствия данного имени
                                                            boolean isFind = false;
                                                            // делаем запрос к базе
                                                            if (query.moveToNext()) {
                                                                if (query.getString(0) == newName) {
                                                                    isFind = true;
                                                                }
                                                            }

                                                            if (newName.isEmpty()) {
                                                                Log.d("NNNG", "empty");
                                                            }

                                                            // проверяем или есть уже такое имя или длина символов = 0
                                                            //if(isFind || newName.isEmpty())
                                                            //{

                                                            //  TextView tv1 = (TextView)findViewById(R.id.tvError);
                                                            //  tv1.setVisibility(View.VISIBLE);
                                                            //  tv1.setText("Данное имя некорректно");
                                                            //}

                                                            //
                                                            db.close();

                                                            //Вводим текст и отображаем в строке ввода на основном экране:
                                                            //final_text.setText(userInput.getText());

                                                            //Log.d("TTT","IDDD: "+holder.id);
                                                            //db.execSQL("UPDATE devices1 SET mac = '"+userInput.getText()+"' WHERE devId="+holder.id);
                                                            //Toast.makeText(mContext, userInput.getText(), Toast.LENGTH_LONG).show();
                                                            //db.close();
                                                            //updateRVadapter();

                                                    }
                                                })
                                        .setNegativeButton("Отмена",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                //Создаем AlertDialog:
                                AlertDialog alertDialog = mDialogBuilder.create();

                                 */

                                //и отображаем его:
                                dialog.show();
                                //Toast.makeText(mContext, "Rename", Toast.LENGTH_LONG).show();
                                break;
                        }
                        return false;
                    }
                });
                menu.show();
            });

        }

        @Override
        public int getItemCount() {
            return listItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            public TextView txtMac;
            public String mac;
            public ViewHolder(View itemView) {
                super(itemView);
                txtMac = (TextView) itemView.findViewById(R.id.boardNameScan);
            }
        }

    }

    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private ViewLeAdapter viewLeDevices;
        //private LayoutInflater mInflator;

        public LeDeviceListAdapter(ViewLeAdapter viewAdapter) {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            viewLeDevices = viewAdapter;
            //mInflator = ScannerActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
                //viewLeDevices.listItems.add((new ScannerActivity.RecyclerItem(device.getName()+" "+device.getAddress(),device.getAddress())));
                viewLeDevices.listItems.add((new ScannerActivity.RecyclerItem("tribo "+device.getAddress(),device.getAddress())));
                //listItems.add(new MainActivity.RecyclerItem(device.getAddress()));
                rv.setAdapter(viewLeDevices);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }
        @Override
        public View getView(int i, View view, ViewGroup viewGroup){

            return view;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        mSearchLayout = (LinearLayout)findViewById(R.id.main_progress1);
        // create recycler view instance
        rv = (RecyclerView) findViewById(R.id.main_list1);
        //rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        // create viewLeDeviceAdapter instance
        vLeAdapter = new ViewLeAdapter(this);
        // get button "search" object
        //AppCompatButton btnScan = (AppCompatButton)findViewById(R.id.search1);
        AppCompatImageView btnScan = (AppCompatImageView)findViewById(R.id.search1);

        //AppCompatButton btnReturn = (AppCompatButton)findViewById(R.id.ret1);
        AppCompatImageView btnReturn = (AppCompatImageView)findViewById(R.id.ret1);
        // get bluetooth manager
        BluetoothManager btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        // get bluetooth adapter
        mBluetoothAdapter = btManager.getAdapter();
        // create LeDevicesAdapter
        mLeDeviceListAdapter = new ScannerActivity.LeDeviceListAdapter(vLeAdapter);
        // get LE Scanner
        bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        // check bt adapter
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        if (mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), "error BT adapter", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        else{
            //Toast.makeText(getApplicationContext(), "BT adapter OK", Toast.LENGTH_SHORT).show();
        }



        // btn "search" click handler
        btnScan.setOnClickListener(this);
        btnReturn.setOnClickListener(this);
        // create handler instance
        mHandler = new Handler();

        rv.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void scanLeDevice(final boolean enable) {
        if (!mScanning) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    //Log.d("TT","stopScan");
                    mSearchLayout.setVisibility(View.INVISIBLE);
                    rv.setVisibility(View.VISIBLE);
                    //mBluetoothAdapter.stopLeScan(leScanCallback);
                    bluetoothLeScanner.stopScan(leCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            Log.e("TT","startSCan");
            //mBluetoothAdapter.startLeScan(leScanCallback);
            ScanSettings settings = new ScanSettings.Builder().setScanMode( ScanSettings.SCAN_MODE_LOW_LATENCY )
                    .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                    .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                    .build();

            bluetoothLeScanner.startScan(null,settings,leCallback);
        } else {
            mScanning = false;
            mSearchLayout.setVisibility(View.INVISIBLE);
            rv.setVisibility(View.VISIBLE);
            //Log.d("TT","stopScan");
            //mBluetoothAdapter.stopLeScan(leScanCallback);
            bluetoothLeScanner.stopScan(leCallback);
        }
    }

    private void stopScan() {
        if(mScanning) {
            if(mBluetoothAdapter !=null && mBluetoothAdapter.isEnabled()){
                //mBluetoothAdapter.stopLeScan(leScanCallback);
                bluetoothLeScanner.stopScan(leCallback);
            }
        }
    }

    // Device scan callback.

    private ScanCallback leCallback = new ScanCallback() {
        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.e("F","FAILEDD");
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    BluetoothDevice device = result.getDevice();
                    //Log.e("TT","dev: "+device.getName()+", mac: "+device.getAddress());
                    if(device.getName() != null && device.getName().indexOf("tribo") != -1){
                    //if(device.getName() != null && device.getName().equalsIgnoreCase("tribo")) {
                        mLeDeviceListAdapter.addDevice(device);
                    }
                    mHandler.sendEmptyMessage(1);
                }
            });
        }
    };

    /*private BluetoothAdapter.LeScanCallback leScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("TT","dev: "+device.getName()+", mac: "+device.getAddress());
                            //if(device.getName() != null && device.getName().indexOf("tribo") != -1){
                            //if(device.getName() != null && device.getName().equalsIgnoreCase("tribo")) {
                                mLeDeviceListAdapter.addDevice(device);
                            //}
                            mHandler.sendEmptyMessage(1);
                        }
                    });
                }
            };*/

}