package pro.tribo.TriboPro;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pro.tribo.TriboPro.classes.BluetoothConnect;
import pro.tribo.TriboPro.classes.DB;
import pro.tribo.TriboPro.classes.RecyclerItem;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //private final String version = "2.000";
    private RecyclerView rv;
    private MainActivity.ViewAdapter vAdapter;
    private String neededAddress = "";
    /****************VIEWS****************/
    LayoutInflater dialogInflater;
    View promptView;
    /*****************DB*****************/
    private DB database;
    public static BluetoothConnect btConnect = null;

    public class ViewAdapter extends RecyclerView.Adapter<MainActivity.ViewAdapter.ViewHolder> {
        public List<RecyclerItem> listItems;
        private Context mContext;
        public ViewAdapter(Context mContext) {
            this.listItems = new ArrayList<>();
            this.mContext = mContext;
        }

        @Override
        public MainActivity.ViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.elem_view, parent, false);
            return new MainActivity.ViewAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final MainActivity.ViewAdapter.ViewHolder holder, final int position) {
            final RecyclerItem itemList = listItems.get(position);
            holder.id = itemList.getId();
            holder.name = itemList.getName();
            holder.txtMac.setText(itemList.getName());
            holder.txtMac.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    PopupMenu menu = new PopupMenu(mContext, holder.txtMac);
                    menu.inflate(R.menu.menu_devices);
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                // connect
                                case R.id.menu1_0:
                                    String mac = database.getMacFromName(holder.txtMac.getText().toString());
                                    neededAddress = mac;
                                    Intent intent = new Intent(mContext, ConnectActivity.class);
                                    intent.putExtra("address",neededAddress);
                                    intent.putExtra("name",holder.name);
                                    startActivityForResult(intent,1);
                                    break;
                                // delete
                                case R.id.menu1_3:
                                    database.deleteDeviceById(holder.id);
                                    updateRVadapter();
                                    break;
                                // get MAC
                                case R.id.menu1_2:
                                    String aMac = database.getMacFromName(holder.txtMac.getText().toString());
                                    Toast.makeText(mContext, aMac, Toast.LENGTH_LONG).show();
                                    break;
                                // Rename
                                case R.id.menu1_1:
                                    final AlertDialog dialog = new AlertDialog.Builder(mContext)
                                            .setView(promptView)
                                            .setPositiveButton("OK", null)
                                            .setNegativeButton("Отмена", null)
                                            .create();
                                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                        @Override
                                        public void onShow(DialogInterface dialog) {
                                            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                                            button.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    final EditText userInput = (EditText) promptView.findViewById(R.id.input_text);
                                                    String newName = userInput.getText().toString();
                                                    if(database.containsName(newName) || newName.isEmpty()){
                                                        TextView tv1 = (TextView)((AlertDialog) dialog).findViewById(R.id.tvError);
                                                        tv1.setVisibility(View.VISIBLE);
                                                        tv1.setText("Данное имя некорректно");
                                                    } else{
                                                        database.updateNameById(holder.id, newName);
                                                        dialog.dismiss();
                                                        updateRVadapter();
                                                    }
                                                }
                                            });
                                        }
                                    });
                                    dialog.show();
                                    break;
                            }
                            return false;
                        }
                    });
                    menu.show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return listItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            public int id;
            public TextView txtMac;
            public String name;
            public ViewHolder(View itemView) {
                super(itemView);
                txtMac = (TextView) itemView.findViewById(R.id.boardName);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {return;}
        int status = data.getIntExtra("resultConnect",0);
        String name = data.getStringExtra("name");
        Log.e("AAA","result: "+status);
        // return from ConnectActivity Success
        if(status == -100) {
            Intent intent = new Intent(this, ContentActivity.class);
            intent.putExtra("name", name);
            startActivityForResult(intent,1);
            Log.e("AAA", "start content");
        }
        // return from ConnectActivity Error
        else if(status == -101) {
            btConnect.disconnect();
            Toast.makeText(this, "Connect Failed", Toast.LENGTH_LONG).show();
        }
        // return from ContentActivity Disconnect
        else if(status == -102) {
            // разкомментировать
            btConnect.disconnect();
            btConnect = null;

            Log.e("hh", "Err -102");
            Toast.makeText(this, "Device Lost", Toast.LENGTH_LONG).show();
        }
        // return from ContentActivity Not Responding
        else if(status == -103) {
            Toast.makeText(this, "Device not responding", Toast.LENGTH_LONG).show();
        }
        // normally close, tray
        else if(status == -104) {
            btConnect.disconnect();
            btConnect = null;
        } else if(status == -105) {

        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("fgfgfg", "Permission Granted! 1");
                } else {
                    Log.e("fgfgfg", "Permission Denied! 1");
                }
                break;
            case 2:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("fgfgfg", "Permission Granted! 2");
                } else {
                    Log.e("fgfgfg", "Permission Denied! 2");
                }
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(this.checkSelfPermission(Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED ||
            this.checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Приложению необходим доступ к Bluetooth");
            builder.setMessage("Пожалуйста, разрешите доступ к Bluetooth для возможности работы приложения");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setCancelable(true);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    requestPermissions(new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN}, 1);
                }
            });
            builder.show();
        }
        //if (Build.VERSION.SDK_INT >= 23) {

            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Приложению необходим доступ к Геолокации");
                builder1.setMessage("Пожалуйста, разрешите доступ к геолокации для возможности работы Bluetooth устройств");
                builder1.setPositiveButton(android.R.string.ok, null);
                builder1.setCancelable(true);
                builder1.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION}, 2);
                        Log.e("HHHH","REQUEST");
                    }
                });
                builder1.show();
            }

            {
            dialogInflater = LayoutInflater.from(this);
            promptView = dialogInflater.inflate(R.layout.prompt_main, null);
            database = new DB(this);
            updateRVadapter();
        }
        AppCompatButton button = (AppCompatButton)findViewById(R.id.search);
        button.setOnClickListener(this);
        AppCompatButton btnMenu = (AppCompatButton)findViewById(R.id.info);
        btnMenu.setOnClickListener(this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        updateRVadapter();
    }

    private void updateRVadapter() {
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("appTribo.db",  MODE_PRIVATE,null);
        Cursor query = db.rawQuery("SELECT * FROM devices1;", null);
        class TempClass {
            public int id;
            public String name;
            public String mac;
        }
        boolean test = false;
        //Log.d("GGG","nums: "+query.getCount());
        ArrayList<TempClass> list = new ArrayList<>();
        while(query.moveToNext()) {
            TempClass temp = new TempClass();
            temp.id = query.getInt(0);
            //Log.d("TTT","ID: "+Integer.toString(temp.id));
            temp.name = query.getString(1);
            if (temp.name.equals("test")) test = true;
            temp.mac = query.getString(2);
            list.add(temp);
        }
        if (!test) db.execSQL("INSERT INTO devices1 (name,mac) VALUES('test','00:00:00:00:00:00')");
        query.close();
        db.close();
        // create recycler view instance
        rv = (RecyclerView) findViewById(R.id.main_list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        // create viewLeDeviceAdapter instance
        vAdapter = new MainActivity.ViewAdapter(this);
        for(int i =0; i<list.size(); i++) {
            vAdapter.listItems.add(new RecyclerItem(list.get(i).id, list.get(i).name,list.get(i).mac));
        }
        rv.setAdapter(vAdapter);
    }

    @Override
    public void onClick(View v) {
        // button "Scan"
        if(v.getId() == R.id.search) {
            Intent intent = new Intent(this, ScannerActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.info) {
            Intent intent = new Intent(this, InfoActivity.class);
            startActivity(intent);
            //Toast.makeText(this, "Version: "+version, Toast.LENGTH_LONG).show();
        }
    }

    public Context getContext() {
        return (Context)this;
    }
}