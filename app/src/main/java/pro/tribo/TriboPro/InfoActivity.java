package pro.tribo.TriboPro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import pro.tribo.TriboPro.R;


public class InfoActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.ret2)
        {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        AppCompatImageView btnReturn = (AppCompatImageView)findViewById(R.id.ret2);
        btnReturn.setOnClickListener(this);
    }
}