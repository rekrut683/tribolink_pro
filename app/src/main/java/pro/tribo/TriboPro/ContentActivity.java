package pro.tribo.TriboPro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import pro.tribo.TriboPro.classes.BluetoothConnect;
import pro.tribo.TriboPro.classes.Commands;
import pro.tribo.TriboPro.classes.DataTimeDevice;
import pro.tribo.TriboPro.classes.FloatDeviceData;
import pro.tribo.TriboPro.classes.TimeDevice;
import pro.tribo.TriboPro.classes.deviceTribo2;
import pro.tribo.TriboPro.classes.logRecord;
import pro.tribo.TriboPro.classes.mode;
import pro.tribo.TriboPro.classes.port;

import static android.text.InputType.TYPE_CLASS_DATETIME;
import static android.text.InputType.TYPE_CLASS_NUMBER;
import static android.text.InputType.TYPE_NUMBER_FLAG_DECIMAL;
import static android.text.InputType.TYPE_NUMBER_FLAG_SIGNED;


public class ContentActivity extends AppCompatActivity implements BluetoothConnect.ConnectCallback {

    public enum SEQ_STATE {
        HELLO,
        GET,
        SET,
        GET_TIME,
        GET_MODE,
        LOOP,
        BYE,
        CMD,
        NONE,
        ACTION,
        GET_VERSION,
        GET_STATE,
        GET_PIN_MODE,
        GET_NEXT_EVENT,
        SET_TIME,
        SET_ADD_WORK,
        SET_START_STOP,
        SET_EVENT_PERIOD,
        SET_CONFIG
    }
    public deviceTribo2 device = new deviceTribo2();
    public SEQ_STATE seqState = SEQ_STATE.HELLO;
    public boolean mStatus = false;
    public Handler mHandler;
    public Handler resendHandler;
    public Handler lostResponseHandler;
    public int TIME_LOOP = 1000;
    public Commands cmd;
    public BluetoothConnect btConnect = MainActivity.btConnect;
    public boolean isRead = false;
    //public byte[] bData;
    public int countError = 0;
    //****************BROADCAST****************************//
    private final String ACTION_NEW_CMD = "action_cmd_send";
    private final String EXTRA_DATA = "extra_data";
    private final String ACTION_READ_DATA = "read_data";
    private final String START_OPERATION = "start_operation";
    // HEAD
    AppCompatImageView btnReturn;
    // INFO
    TextView tv_info_mode, tv_info_input1, tv_info_input2, tv_info_input3, tv_info_input4;
    TextView tv_info_input1_name, tv_info_input2_name, tv_info_input3_name, tv_info_input4_name;
    TextView tv_info_remainTimeH, tv_info_remainTimeM, tv_info_remainTimeS;
    TextView tv_info_current, tv_info_voltageIn, tv_info_voltageOut, tv_info_addStart;
    TextView tv_info_reallyTimeH, tv_info_reallyTimeM, tv_info_reallyTimeS, tv_info_pause;
    TextView tv_info_state, tv_info_currentCycle;
    TableRow tr_info_currentCycle, tr_info_input1, tr_info_input2, tr_info_input3, tr_info_input4;
    // SERVICE
    EditText et_service_datetime, et_service_password;
    TextView tv_service_version;
    // PORTS
    EditText et_ports_setTimeWork, et_ports_setTimePeriod;
    TextView tv_ports_input1, tv_ports_input2, tv_ports_input3, tv_ports_input4, tv_ports_input5,
            tv_ports_applay, tv_ports_cancel, tv_ports_cicles;
    TableRow tr_ports_cycles;
    // PORT
    EditText et_port_name, et_port_measurementMin, et_port_measurementMax, et_port_rangeMin,
    et_port_rangeMax, et_port_unit, et_port_alarmMin, et_port_alarmMax, et_port_timeDelayMin,
            et_port_timeDelayMax;
    TextView tv_port_in, tv_port_applay, tv_port_cancel;
    TableRow tr_port_viewSensor, tr_port_typeSensor, tr_port_normSensor, tr_port_minMeasure,
    tr_port_maxMeasure, tr_port_minRange, tr_port_maxRange, tr_port_unit, tr_port_range,
    tr_port_minAlarm, tr_port_minAlarmReaction, tr_port_maxAlarm, tr_port_maxAlarmReaction,
    tr_port_timeDelayMin, tr_port_timeDelayMax, tr_port_typeLevelSensor;
    Spinner s_port_typeLevelSensor, s_port_inputType, s_port_sensor, s_port_sensorType,
            s_port_sensorBeginState, s_port_actionLess, s_port_actionMore, s_ports_mode;
    // LOG
    EditText et_log_datetime1, et_log_datetime2;
    TextView tv_log_applay, tv_log_clear, tv_log_next, tv_log_out;
    // FOOTER
    BottomNavigationView BottomNavigationView;
    // OTHER
    AppCompatEditText timeWorkHourView, timeWorkMinuteView, timeWorkSecView, timePauseHourView;
    AppCompatEditText timePauseMinuteView, timePauseSecView, cyclesCounterView;
    TextView viewName;
    AlertDialog dialogWait;
    byte[] resendData;
    ArrayAdapter<?>  adapterSpinnerMode, adapterInputType, adapterSensor,
            adapterSensorType, adapterBeginState, adapterActionLess, adapterActionMore,
            adapterSpinnerTypeLevelSensor;
    LinearLayout ll_old, ll_Wait, ll_service, ll_Info, ll_Ports, ll_Port, ll_Log;
    /****************LOG**************************/
    String logPath = "/Tribo/log.txt";
    /********************ACTIVITY********************/
    /***********************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // HEAD
        btnReturn = (AppCompatImageView) findViewById(R.id.ret_from_details);
        btnReturn.setOnClickListener(returnListener);
        viewName = (TextView) findViewById(R.id.device_name_detail);
        // INFO
        tv_info_mode = (TextView) findViewById(R.id.tv_info_mode);
        tv_info_reallyTimeH = (TextView) findViewById(R.id.et_info_setTimePeriodH);
        tv_info_reallyTimeM = (TextView) findViewById(R.id.et_info_setTimePeriodM);
        tv_info_reallyTimeS = (TextView) findViewById(R.id.et_info_setTimePeriodS);
        tv_info_state = (TextView) findViewById(R.id.tv_info_state);
        tv_info_remainTimeH = (TextView) findViewById(R.id.tv_info_remainTimeH);
        tv_info_remainTimeM = (TextView) findViewById(R.id.tv_info_remainTimeM);
        tv_info_remainTimeS = (TextView) findViewById(R.id.tv_info_remainTimeS);
        tv_info_currentCycle = (TextView) findViewById(R.id.tv_info_currentCycle);
        tv_info_input1_name = (TextView)findViewById(R.id.tv_info_input1_name);
        tv_info_input2_name = (TextView)findViewById(R.id.tv_info_input2_name);
        tv_info_input3_name = (TextView)findViewById(R.id.tv_info_input3_name);
        tv_info_input4_name = (TextView)findViewById(R.id.tv_info_input4_name);
        tv_info_input1 = (TextView) findViewById(R.id.tv_info_input1);
        tv_info_input2 = (TextView) findViewById(R.id.tv_info_input2);
        tv_info_input3 = (TextView) findViewById(R.id.tv_info_input3);
        tv_info_input4 = (TextView) findViewById(R.id.tv_info_input4);
        tv_info_current = (TextView) findViewById(R.id.tv_info_current);
        tv_info_voltageIn = (TextView) findViewById(R.id.tv_info_voltageIn);
        tv_info_voltageOut = (TextView) findViewById(R.id.tv_info_voltageOut);
        tv_info_addStart = (TextView) findViewById(R.id.tv_info_addStart);
        tv_info_pause = (TextView) findViewById(R.id.tv_info_pause);
        tr_info_currentCycle = (TableRow) findViewById(R.id.tr_info_currentCycle);
        tr_info_input1 = (TableRow) findViewById(R.id.tr_info_input1);
        tr_info_input2 = (TableRow) findViewById(R.id.tr_info_input2);
        tr_info_input3 = (TableRow) findViewById(R.id.tr_info_input3);
        tr_info_input4 = (TableRow) findViewById(R.id.tr_info_input4);
        tv_info_addStart.setOnClickListener(infoAddWorkCycleListener);
        tv_info_pause.setOnClickListener(infoStartStopListener);
        // SERVICE
        et_service_datetime = (EditText) findViewById(R.id.et_service_datetime);
        et_service_password = (EditText) findViewById(R.id.et_service_password);
        tv_service_version = (TextView) findViewById(R.id.tv_service_version);
        et_service_datetime.setOnClickListener(serviceDataTimeListener);
        et_service_password.setOnClickListener(servicePasswordListener);
        // PORTS
        s_ports_mode = (Spinner) findViewById(R.id.s_ports_mode);
        et_ports_setTimeWork = (EditText) findViewById(R.id.et_ports_setTimeWork);
        et_ports_setTimePeriod = (EditText) findViewById(R.id.et_ports_setTimePeriod);
        tv_ports_cicles = (TextView) findViewById(R.id.tv_ports_cicles);
        tv_ports_input1 = (TextView) findViewById(R.id.tv_ports_input1);
        tv_ports_input2 = (TextView) findViewById(R.id.tv_ports_input2);
        tv_ports_input3 = (TextView) findViewById(R.id.tv_ports_input3);
        tv_ports_input4 = (TextView) findViewById(R.id.tv_ports_input4);
        tv_ports_input5 = (TextView) findViewById(R.id.tv_ports_input5);
        tv_ports_cancel = (TextView) findViewById(R.id.tv_ports_cancel);
        tv_ports_applay = (TextView) findViewById(R.id.tv_ports_applay);
        tv_ports_cancel.setOnClickListener(portsCancelListener);
        tv_ports_applay.setOnClickListener(portsApplayListener);
        tr_ports_cycles = (TableRow) findViewById(R.id.tr_ports_cycles);
        tv_ports_input1.setOnClickListener(portsConfigPortListener);
        tv_ports_input2.setOnClickListener(portsConfigPortListener);
        tv_ports_input3.setOnClickListener(portsConfigPortListener);
        tv_ports_input4.setOnClickListener(portsConfigPortListener);
        tv_ports_input5.setOnClickListener(portsConfigPortListener);
        et_ports_setTimeWork.setOnClickListener(timeIntervalListener);
        et_ports_setTimePeriod.setOnClickListener(timeIntervalListener);
        tv_ports_cicles.setOnClickListener(integerListener);
        // PORT
        tv_port_cancel = (TextView) findViewById(R.id.tv_port_cancel);
        tv_port_applay = (TextView) findViewById(R.id.tv_port_applay);
        tv_port_in = (TextView) findViewById(R.id.tv_port_in);
        et_port_name = (EditText) findViewById(R.id.et_port_name);
        et_port_measurementMin = (EditText) findViewById(R.id.et_port_measurementMin);
        et_port_measurementMax = (EditText) findViewById(R.id.et_port_measurementMax);
        et_port_rangeMin = (EditText) findViewById(R.id.et_port_rangeMin);
        et_port_rangeMax = (EditText) findViewById(R.id.et_port_rangeMax);
        et_port_unit = (EditText) findViewById(R.id.et_port_unit);
        et_port_alarmMin = (EditText) findViewById(R.id.et_port_alarmMin);
        et_port_alarmMax = (EditText) findViewById(R.id.et_port_alarmMax);
        et_port_timeDelayMin = (EditText) findViewById(R.id.et_port_timeDelayMin);
        et_port_timeDelayMax = (EditText) findViewById(R.id.et_port_timeDelayMax);
        s_port_inputType = (Spinner) findViewById(R.id.s_port_inputType);
        s_port_sensor = (Spinner) findViewById(R.id.s_port_sensor);
        s_port_sensorType = (Spinner) findViewById(R.id.s_port_sensorType);
        s_port_sensorBeginState = (Spinner) findViewById(R.id.s_port_sensorBeginState);
        s_port_actionLess = (Spinner) findViewById(R.id.s_port_actionLess);
        s_port_actionMore = (Spinner) findViewById(R.id.s_port_actionMore);
        s_port_typeLevelSensor  = (Spinner) findViewById(R.id.s_port_typeLevelSensor);
        tv_port_applay.setOnClickListener(portApplayListener);
        tv_port_cancel.setOnClickListener(portCancelListener);
        tr_port_viewSensor = (TableRow) findViewById(R.id.tr_port_viewSensor);
        tr_port_typeSensor = (TableRow) findViewById(R.id.tr_port_typeSensor);
        tr_port_typeLevelSensor = (TableRow) findViewById(R.id.tr_port_typeLevelSensor);
        tr_port_normSensor = (TableRow) findViewById(R.id.tr_port_normSensor);
        tr_port_minMeasure = (TableRow) findViewById(R.id.tr_port_minMeasure);
        tr_port_maxMeasure = (TableRow) findViewById(R.id.tr_port_maxMeasure);
        tr_port_minRange = (TableRow) findViewById(R.id.tr_port_minRange);
        tr_port_maxRange = (TableRow) findViewById(R.id.tr_port_maxRange);
        tr_port_unit = (TableRow) findViewById(R.id.tr_port_unit);
        tr_port_range = (TableRow) findViewById(R.id.tr_port_range);
        tr_port_minAlarm = (TableRow) findViewById(R.id.tr_port_minAlarm);
        tr_port_timeDelayMin = (TableRow) findViewById(R.id.tr_port_timeDelayMin);
        tr_port_minAlarmReaction = (TableRow) findViewById(R.id.tr_port_minAlarmReaction);
        tr_port_maxAlarm = (TableRow) findViewById(R.id.tr_port_maxAlarm);
        tr_port_timeDelayMax = (TableRow) findViewById(R.id.tr_port_timeDelayMax);
        tr_port_maxAlarmReaction = (TableRow) findViewById(R.id.tr_port_maxAlarmReaction);
        et_port_timeDelayMin.setOnClickListener(timeIntervalListener);
        et_port_timeDelayMax.setOnClickListener(timeIntervalListener);
        et_port_measurementMin.setOnClickListener(floatListener);
        et_port_measurementMax.setOnClickListener(floatListener);
        et_port_rangeMin.setOnClickListener(floatListener);
        et_port_rangeMax.setOnClickListener(floatListener);
        et_port_alarmMin.setOnClickListener(floatListener);
        et_port_alarmMax.setOnClickListener(floatListener);
        et_port_name.setOnClickListener(textListener);
        et_port_unit.setOnClickListener(textListener);
        // LOG
        et_log_datetime1 = (EditText) findViewById(R.id.et_log_datetime1);
        et_log_datetime2 = (EditText) findViewById(R.id.et_log_datetime2);
        tv_log_applay = (TextView) findViewById(R.id.tv_log_applay);
        tv_log_clear = (TextView) findViewById(R.id.tv_log_clear);
        tv_log_next = (TextView) findViewById(R.id.tv_log_next);
        tv_log_out = (TextView) findViewById(R.id.tv_log_out);
        tv_log_next.setOnClickListener(logNextListener);
        tv_log_applay.setOnClickListener(logApplayListener);
        tv_log_clear.setOnClickListener(logClearListener);
        et_log_datetime1.setOnClickListener(dateTimeListener);
        et_log_datetime2.setOnClickListener(dateTimeListener);
        //
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        viewName.setText(name);
        // common variables
        mHandler = new Handler();
        resendHandler = new Handler();
        lostResponseHandler = new Handler();
        cmd = new Commands();
        //
        ll_Wait = (LinearLayout) findViewById(R.id.view_wait);
        ll_Info = (LinearLayout) findViewById(R.id.view_info);
        ll_old = (LinearLayout) ll_Info;
        ll_service = (LinearLayout) findViewById(R.id.view_service);
        ll_Ports = (LinearLayout) findViewById(R.id.view_ports);
        ll_Ports.setTag(ll_Ports);
        ll_Port = (LinearLayout) findViewById(R.id.view_port);
        ll_Log = (LinearLayout) findViewById(R.id.view_log);
        //
        BottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_info:
                        ll_old.setVisibility(View.GONE);
                        ll_Info.setVisibility(View.VISIBLE);
                        ll_old = ll_Info;
                        item.setChecked(true);
                        try {
                            showInfo();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.navigation_service:
                        ll_old.setVisibility(View.GONE);
                        ll_service.setVisibility(View.VISIBLE);
                        ll_old = ll_service;
                        showService();
                        item.setChecked(true);
                        break;
                    case R.id.navigation_ports:
                        ll_old.setVisibility(View.GONE);
                        ll_old = (LinearLayout) ll_Ports.getTag();
                        ll_old.setVisibility(View.VISIBLE);
                        item.setChecked(true);
                        try {
                            showPorts();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.navigation_log:
                        ll_old.setVisibility(View.GONE);
                        ll_Log.setVisibility(View.VISIBLE);
                        ll_old = ll_Log;
                        showLog();
                        item.setChecked(true);
                        break;
                }
                return false;
            }
        });

        //***************PORTS***MODE***SPINNER***************//
        adapterSpinnerMode = ArrayAdapter.createFromResource(this, R.array.mode, R.layout.spinner);
        adapterSpinnerMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_ports_mode.setAdapter(adapterSpinnerMode);
        s_ports_mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                if (selectedItemPosition == 2) {
                    tr_ports_cycles.setVisibility(View.VISIBLE);
                }  else {
                    tr_ports_cycles.setVisibility(View.GONE);
                }
                if (selectedItemPosition != device.mode.mode) {
                    portsControlVisible(true);
                    device.newMode.mode = (byte) selectedItemPosition;
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.GREEN);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //***************PORT***INPUT_TYPE***SPINNER***************//
        adapterInputType = ArrayAdapter.createFromResource(this, R.array.inputType, R.layout.spinner);
        adapterInputType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_port_inputType.setAdapter(adapterInputType);
        s_port_inputType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                try {
                    showPort();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //***************PORT***VIEW_SENSOR***SPINNER***************//
        adapterSensor = ArrayAdapter.createFromResource(this, R.array.sensor, R.layout.spinner);
        adapterSensor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_port_sensor.setAdapter(adapterSensor);
        s_port_sensor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                try {
                    showPort();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //***************PORT***TYPE_SENSOR***SPINNER***************//
        adapterSensorType = ArrayAdapter.createFromResource(this, R.array.sensorType, R.layout.spinner);
        adapterSensorType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_port_sensorType.setAdapter(adapterSensorType);
        s_port_sensorType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                try {
                    showPort();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //***************PORT***NORM_STATE***SPINNER***************//
        adapterBeginState = ArrayAdapter.createFromResource(this, R.array.sensorBeginState, R.layout.spinner);
        adapterBeginState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_port_sensorBeginState.setAdapter(adapterBeginState);
        s_port_sensorBeginState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                try {
                    showPort();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //***************PORT***TYPE_LEVEL_SENSOR***SPINNER***************//
        adapterSpinnerTypeLevelSensor = ArrayAdapter.createFromResource(this, R.array.levelType, R.layout.spinner);
        adapterSpinnerTypeLevelSensor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_port_typeLevelSensor.setAdapter(adapterSpinnerTypeLevelSensor);
        s_port_typeLevelSensor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                try {
                    showPort();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //***************PORT***REACTION_LESS***SPINNER***************//
        adapterActionLess = ArrayAdapter.createFromResource(this, R.array.action, R.layout.spinner);
        adapterActionLess.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_port_actionLess.setAdapter(adapterActionLess);
        s_port_actionLess.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                try {
                    showPort();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //***************PORT***REACTION_ABOVE***SPINNER***************//
        adapterActionMore = ArrayAdapter.createFromResource(this, R.array.action, R.layout.spinner);
        adapterActionMore.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s_port_actionMore.setAdapter(adapterActionMore);
        s_port_actionMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                try {
                    showPort();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        tv_log_next.setClickable(false);
        portsControlVisible(false);

        // subscribe for bluetooth device callback
        MainActivity.btConnect.SubscribeCallback(this);
        // register broadcast receiver
        registerReceiver(mGattUpdateReceiver, broadcastFilter());
        // send "handshake" data to bt device
        broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.ACTION, cmd.HELLO,null));
        //
        try {
            showInfo();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.e("AAA", "we are here");
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            BottomNavigationView.setVisibility(View.VISIBLE);
            View decorView = this.getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        } else {
            BottomNavigationView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            EditText v = (EditText) getCurrentFocus();
            if (v != null) v.clearFocus();
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (ll_old == ll_Port) {
                ll_Ports.setTag(ll_Ports);
                BottomNavigationView.setSelectedItemId(R.id.navigation_ports);
            } else if (ll_old == ll_service || ll_old == ll_Ports || ll_old == ll_Log) {
                BottomNavigationView.setSelectedItemId(R.id.navigation_info);
            } else close();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeActivity(-104);
    }

    @Override
    public void onBackPressed() {
        onStop();
    }

    private void close() {
        if (btConnect != null) {
            btConnect.disconnect();
        }
        endWork();
        finish();
        return;
    }

    public Context getContext() {
        return (Context) this;
    }

    private String getDateTime() {
        Date c = Calendar.getInstance().getTime();
        //System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    private void writeLog(String msg) {
        //Log.e("CC",Environment.getExternalStorageDirectory().toString()+logPath);
        try {
            String dirPath = new String();
            dirPath = Environment.getExternalStorageDirectory().toString() + File.separator + "Tribo";
            File folder = new File(dirPath);
            //Log.e("IO","path: "+dirPath);
            if (!folder.exists()) {
                folder.mkdir();
                //Log.e("IO", ""+folder.exists());
            } else {
                //Log.e("IO","dir exist");
            }
            FileOutputStream outputStream = new FileOutputStream(dirPath + File.separator + "log.txt", true);
            StringBuilder sb = new StringBuilder();
            sb.append(getDateTime());
            sb.append(" ");
            sb.append(msg);
            sb.append("\r\n");
            outputStream.write(sb.toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            //Log.e("r","ERROR :"+e.toString());
            e.printStackTrace();
        }
    }

    private void closeActivity(int code) {
        Intent intent = new Intent();
        intent.putExtra("resultConnect", code);
        //MainActivity.btConnect = this.btConnect;
        endWork();
        setResult(RESULT_OK, intent);
        if (btConnect != null) {
            btConnect.disconnect();
        }
        //unregisterReceiver(mGattUpdateReceiver);
        finish();
        return;
    }

    private void endWork() {
        seqState = SEQ_STATE.NONE;
        stopLoop();
        lostResponseHandler.removeMessages(0);
        countError = 0;
        Log.e("Tribo2", "END WORK");
    }

    /****************CHANGE VIEWS DATA*****************/
    private void ChangeCycleCounterView(byte mode) {
        cyclesCounterView.setText(Byte.toString(mode));
    }

    /*******************HELPER METHODS***********************/
    final Runnable runnable = new Runnable() { // вызывается раз в секунду
        public void run() {
            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET, cmd.STATE, null));
            /****************************************/
            mHandler.postDelayed(this, TIME_LOOP);
        }
    };

    final Runnable lostResponseMonitor = new Runnable() {
        @Override
        public void run() {
            if (dialogWait != null) {
                dialogWait.dismiss();
            }
            countError++;
            Log.e("Tribo2", "error: " + countError);
            if (countError == 5) {
                Log.e("Tribo2", "Error: - 102");
                closeActivity(-102);
                //mHandler.removeMessages(0);
                countError = 0;
                return;
            } else {
                broadcastUpdate(ACTION_NEW_CMD, resendData);
            }
        }
    };

    /*********************LOOP**************************/
    private void stopLoop() {
        mHandler.removeMessages(0);
    }

    private void startLoop() {
        mHandler.postDelayed(runnable, TIME_LOOP);
    }
    /****************BROADCAST_RECEIVER****************/
    private IntentFilter broadcastFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_NEW_CMD);
        intentFilter.addAction(ACTION_READ_DATA);
        intentFilter.addAction(START_OPERATION);
        return intentFilter;
    }

    private void broadcastUpdate(String msg, byte[] data) {
        final Intent intent = new Intent(msg);
        intent.putExtra(EXTRA_DATA, data);
        sendBroadcast(intent);
    }

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                final String action = intent.getAction();
                byte[] payload = null;
                // отправка данных в устройство
                if (action.equals(ACTION_NEW_CMD)) {
                    // Pause
                    try {
                        Thread.sleep(75);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // get data
                    byte[] data = intent.getByteArrayExtra(EXTRA_DATA);

                    if (data.length > 20) {
                        byte addr = 0;
                        byte len;
                        byte[] currentBlock;
                        while ((data.length - addr) > 0) {
                            len = (byte) (data.length - addr);
                            len = (len > 20 ? 20 : len);
                            currentBlock = new byte[len];
                            System.arraycopy(data, addr, currentBlock, 0, len);
                            btConnect.isSend = true;                                                // Костыль (асинхронный вызов)
                            btConnect.WriteBytes(currentBlock);
                            while (btConnect.isSend);
                            addr += 20;
                        }

                    } else btConnect.WriteBytes(data);                                              // write bytes to Device
                    // save last data
                    resendData = data;
                    // start monitor "lost response"
                    lostResponseHandler.postDelayed(lostResponseMonitor, 3300);
                }
                // новая команда на устройство
                else if (action.equals(START_OPERATION)) {
                    // loop get state Stopped
                    stopLoop();
                    removeTimeout();
                    // start alert dialog
                    LayoutInflater li = LayoutInflater.from(getContext());
                    View promptsView = li.inflate(R.layout.operation, null);
                    dialogWait = new AlertDialog.Builder(getContext()).setView(promptsView).create();
                    dialogWait.setCanceledOnTouchOutside(false);
                    dialogWait.show();
                    // Get Data
                    byte[] data = intent.getByteArrayExtra(EXTRA_DATA);
                    // Send command
                    broadcastUpdate(ACTION_NEW_CMD, data);
                }
                // ответ от устройства
                else if (action.equals(ACTION_READ_DATA)) {
                    switch (seqState) {
                        case NONE: {
                            break;
                        }
                        case SET_EVENT_PERIOD: {
                            if (cmd.currentCmd[0] == cmd.SET && cmd.currentCmd[1] == cmd.EVENT_RANGE) {
                                removeTimeout();
                                logRecord lRec = new logRecord(cmd.getCurrentData());
                                tv_log_out.setText(lRec.record);
                                if (cmd.getLogEnd() == false) {
                                    tv_log_next.setClickable(true);
                                    tv_log_next.setTextColor(Color.GREEN);
                                } else {
                                    tv_log_next.setClickable(false);
                                    tv_log_next.setTextColor(Color.GRAY);
                                }
                                setSeqStateLoop();
                            }
                            break;
                        }
                        case GET_NEXT_EVENT: {
                            if (cmd.currentCmd[0] == cmd.GET && cmd.currentCmd[1] == cmd.NEXT_EVENT) {
                                removeTimeout();
                                logRecord lRec = new logRecord(cmd.getCurrentData());
                                tv_log_out.setText(tv_log_out.getText() + lRec.record);
                                if (cmd.getLogEnd() == false) {
                                    tv_log_next.setClickable(true);
                                    tv_log_next.setTextColor(Color.GREEN);
                                } else {
                                    tv_log_next.setClickable(false);
                                    tv_log_next.setTextColor(Color.GRAY);
                                }
                                setSeqStateLoop();
                            }
                            break;
                        }
                        case SET_ADD_WORK: {
                            removeTimeout();
                            if (cmd.getResponseAddWork()) {

                            }
                            setSeqStateLoop();
                            break;
                        }
                        case SET_START_STOP: {
                            removeTimeout();
                            if (cmd.getResponseStartStop()) {

                            }
                            setSeqStateLoop();
                            break;
                        }
                        case SET_CONFIG: {
                            removeTimeout();
                            if (cmd.currentCmd[0] == cmd.SET) {
                                switch (device.sequenceSetConfig) {
                                    case 0: { // Должна прийти команда стоп
                                        device.sequenceSetConfig++;
                                        if (!device.mode.compare(device.newMode)) {
                                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.SET, cmd.TIME_MODE, device.newMode.getMode()));
                                            break;
                                        }
                                    }
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6: { // Должна прийти команда SET_MODE
                                        for (byte i = device.sequenceSetConfig; device.sequenceSetConfig < 6; device.sequenceSetConfig++){
                                            if (device.newPorts.port[device.sequenceSetConfig-1].compare(device.ports.port[device.sequenceSetConfig-1])) continue;
                                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.SET, cmd.PIN_MODE, device.newPorts.port[i-1].getPort((byte)(i-1))));
                                            device.sequenceSetConfig++;
                                            break;
                                        }
                                        if (device.sequenceSetConfig > 5) {
                                            seqState = SEQ_STATE.HELLO;
                                            // send GET_TIME request
                                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.SET, cmd.PAUSE_INV, null));
                                            device.sequenceSetConfig = 0;
                                            break;
                                        }
                                        break;
                                    }
                                    default: {
                                        setSeqStateLoop();
                                        break;
                                    }
                                }
                            }
                            break;
                        }

                        case SET: {
                            lostResponseHandler.removeMessages(0);
                            countError = 0;
                            // вызываем на cmd парсер ответа на SET



/*                            if (cmd.getExtra() == cmd.) {
                                if ((cmd.getExtra() == cmd.EVENT_RANGE) | (cmd.getExtra() == cmd.GET_NEXT_EVENT)) {
                                    logRecord lRec = new logRecord(cmd.currentCmd);
                                    tv_log_out.setText(lRec.record);
                                    if (cmd.getLogEnd() == false) {
                                        tv_log_next.setClickable(true);
                                        tv_log_next.setTextColor(Color.GREEN);
                                    } else {
                                        tv_log_next.setClickable(false);
                                        tv_log_next.setTextColor(Color.GRAY);
                                    }

*/
                            seqState = SEQ_STATE.GET_STATE;
                            // send GET_TIME request
                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET, cmd.TIME_MODE, null));

                            break;
                        }
                        case GET: {
                            removeTimeout();            // reset LostResponse Monitor
                            // вызываем на cmd парсер ответа на GET
                            seqState = SEQ_STATE.GET_STATE;
                            // send GET_TIME request
                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET, cmd.TIME_MODE, null));
                            break;
                        }
                        case HELLO: {
                            removeTimeout();
                            seqState = SEQ_STATE.GET_VERSION;
                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET, cmd.VERSION, null));
                            break;
                        }
                        case GET_VERSION: {
                            removeTimeout();
                            device.version =  new String(cmd.getCurrentData(), "UTF-8");
                            tv_service_version.setText(device.version);
                            seqState = SEQ_STATE.GET_MODE;
                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET, cmd.TIME_MODE, null));
                            break;
                        }
                        case GET_MODE: {    // Пакет о режиме
                            removeTimeout();
                            device.mode = new mode(cmd.getCurrentData());
                            device.newMode = new mode(cmd.getCurrentData());
                            seqState = SEQ_STATE.GET_PIN_MODE;
                            device.ports.seqCounter = 0;
                            byte[] data = new byte[1];
                            data[0] = device.ports.seqCounter;
                            broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET, cmd.PIN_MODE, data));
                            break;
                        }
                        case GET_PIN_MODE: {    // Пакет о порте
                            removeTimeout();
                            if (++device.ports.seqCounter < 5) {
                                device.ports.port[device.ports.seqCounter-1] = new port(cmd.getCurrentData());
                                device.newPorts.port[device.ports.seqCounter-1] = new port(cmd.getCurrentData());
                                byte[] data = new byte[1];
                                data[0] = device.ports.seqCounter;
                                broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET, cmd.PIN_MODE, data));
                                return;
                            }
                            device.ports.port[device.ports.seqCounter-1] = new port(cmd.getCurrentData());
                            device.newPorts.port[device.ports.seqCounter-1] = new port(cmd.getCurrentData());
                            ll_Wait.setVisibility(View.GONE);
                            ll_Info.setVisibility(View.VISIBLE);
                            ll_old = ll_Info;
                            device.edited = false;
                            device.newMode.copyMode(device.mode);
                            device.newPorts.copyPorts(device.ports);
                            showInfo();
                            setSeqStateLoop();
                            break;
                        }
                        case LOOP: {
                            removeTimeout();
                            String str = "";
                                device.newState = new deviceTribo2.state(cmd.currentCmd);
                                /********MODE************/
                                if (device.state.mode != device.newState.mode) {
                                    device.state.mode = device.newState.mode;
                                    tv_info_mode.setText(device.state.mode);


                                }
                                /********REALLY_TIME*******/
                                if (!device.state.timeDevice.compare(device.newState.timeDevice)) {
                                    device.state.timeDevice.set(device.newState.timeDevice);
                                    tv_info_reallyTimeH.setText(device.state.timeDevice.getStringHours());
                                    tv_info_reallyTimeM.setText(device.state.timeDevice.getStringMinutes());
                                    tv_info_reallyTimeS.setText(device.state.timeDevice.getStringSeconds());
                                    Log.e("Tribo2Read", Integer.toString(cmd.currentCmd[0]));
                                }
                                /********STATE**********/
                                if (device.state.state != device.newState.state) {
                                    device.state.state = device.newState.state;
                                    switch (device.state.state) {
                                        case 0: {
                                            tv_info_state.setText("Пауза");
                                            tv_info_state.setTextColor(Color.YELLOW);
                                            tv_info_pause.setText("ВЫКЛ");
                                            tv_info_pause.setTextColor(Color.RED);
                                            tv_info_addStart.setClickable(true);
                                            tv_info_addStart.setTextColor(Color.GREEN);
                                            break;
                                        }
                                        case 1: {
                                            tv_info_state.setText("Работа");
                                            tv_info_state.setTextColor(Color.GREEN);
                                            tv_info_pause.setText("ВЫКЛ");
                                            tv_info_pause.setTextColor(Color.RED);
                                            tv_info_addStart.setClickable(false);
                                            tv_info_addStart.setTextColor(Color.GRAY);
                                            break;
                                        }
                                        case 2: {
                                            tv_info_state.setText("Стоп");
                                            tv_info_state.setTextColor(Color.YELLOW);
                                            tv_info_pause.setText("ВКЛ");
                                            tv_info_pause.setTextColor(Color.GREEN);
                                            break;
                                        }
                                        case 3:
                                            if ((device.newState.alarmCode & 0x4) != 0)
                                                str = str + 'L';
                                            if ((device.newState.alarmCode & 0x2) != 0)
                                                str = str + 'C';
                                            if ((device.newState.alarmCode & 0x1) != 0)
                                                str = str + 'I';
                                            tv_info_state.setTextColor(Color.RED);
                                            tv_info_state.setText("Авария" + " (" + str + ")");
                                            tv_info_pause.setText("ВКЛ");
                                            tv_info_pause.setTextColor(Color.GREEN);
                                    }
                                }
                                /********TIME_REMAIN*******/
                                if (!device.state.timeRemain.compare(device.newState.timeRemain)) {
                                    device.state.timeRemain.set(device.newState.timeRemain);
                                    tv_info_remainTimeH.setText(device.state.timeRemain.getStringHour());
                                    tv_info_remainTimeM.setText(device.state.timeRemain.getStringMinute());
                                    tv_info_remainTimeS.setText(device.state.timeRemain.getStringSeconds());
                                }
                                /********CYCLE**********/
                                if (device.state.actualCycle != device.newState.actualCycle) {
                                    device.state.actualCycle = device.newState.actualCycle;
                                    tv_info_currentCycle.setText(Byte.toString(device.state.actualCycle));
                                }
                                /********INPUT1********/
                                if (device.state.input1.data != device.newState.input1.data) {
                                    device.state.input1 = device.newState.input1;
                                    tv_info_input1.setText(device.state.input1.getString(1) + " %");
                                }
                                /********INPUT2**********/
                                if (device.state.input2.data != device.newState.input2.data) {
                                    device.state.input2 = device.newState.input2;
                                    tv_info_input2.setText(device.state.input2.getString(1) + " БАР");

                                }
                                /********INPUT3**********/
                                if (device.state.input3.data != device.newState.input3.data) {
                                    device.state.input3 = device.newState.input3;
                                    tv_info_input3.setText(device.state.input3.getString(1) + " °C");
                                }
                                /********INPUT4**********/
                                if (device.state.input4.data != device.newState.input4.data) {
                                    device.state.input4 = device.newState.input4;
                                    tv_info_input4.setText(device.state.input4.getString(1) + " °C");
                                }
                                /********CURRENT**********/
                                if (device.state.current.data != device.newState.current.data) {
                                    device.state.current = device.newState.current;
                                    tv_info_current.setText(device.state.current.getString(10) + " A");
                                }
                                /********VOLTAGE_IN**********/
                                if (device.state.voltageIn.data != device.newState.voltageIn.data) {
                                    device.state.voltageIn = device.newState.voltageIn;
                                    tv_info_voltageIn.setText(device.state.voltageIn.getString(10) + " B");
                                }
                                /********VOLTAGE_OUT**********/
                                if (device.state.voltageOut.data != device.newState.voltageOut.data) {
                                    device.state.voltageOut = device.newState.voltageOut;
                                    tv_info_voltageOut.setText(device.state.voltageOut.getString(10) + " B");
                                }
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("Tribo2loop", e.toString());
            }
        }
    };

    /************CALLBACKS FROM BT MODULE***********/
    /*******CONNECT DEVICE*********/
    @Override
    public void onConnectCallback(boolean status) {
    }
    /******DISCONNECT DEVICE**********/
    @Override
    public void onDisconnectCallback(boolean status) {
        closeActivity(-105);
    }
    /*******READ DATA FROM DEVICE * @param data*******/
    @Override
    public void onReadCallback(byte[] data) {
        cmd.currentCmd = data;
        isRead = true;
        broadcastUpdate(ACTION_READ_DATA, data);
    }

    public void removeTimeout() {
        lostResponseHandler.removeMessages(0);
        resendData = null;
        countError = 0;
        if(dialogWait != null) {
            dialogWait.dismiss();
        }
    }

    public void setSeqStateLoop() {
        seqState = SEQ_STATE.LOOP;
        broadcastUpdate(ACTION_NEW_CMD, cmd.Constructor(cmd.GET,cmd.STATE, null));
        startLoop();
    }

    OnClickListener returnListener = new OnClickListener() {
        @SuppressLint("ResourceType")
        @Override
        public void onClick(View v) {
            if (ll_old == ll_Port) {
                ll_Ports.setTag(ll_Ports);
                BottomNavigationView.setSelectedItemId(R.id.navigation_ports);
            } else if (ll_old == ll_service || ll_old == ll_Ports || ll_old == ll_Log) {
                BottomNavigationView.setSelectedItemId(R.id.navigation_info);
            } else close();
        }
    };

    OnClickListener infoAddWorkCycleListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            seqState = SEQ_STATE.SET_ADD_WORK;
            broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.SET, cmd.ADD_WORK, null));
        }
    };

    OnClickListener infoStartStopListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            seqState = SEQ_STATE.SET_START_STOP;
            broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.SET, cmd.PAUSE_INV, null));
        }
    };
    public boolean isDataEdited = false;                                                            // триггер для исключения замыкания
    public char[] preStr;
    OnClickListener serviceDataTimeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(ContentActivity.this);
            alert.setTitle("             Установка даты");

            EditText input = new EditText(ContentActivity.this);
            input.setGravity(Gravity.CENTER);
            input.setInputType(TYPE_CLASS_DATETIME);
            alert.setView(input);
            input.setText(et_service_datetime.getText());
            input.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    preStr = s.toString().toCharArray();
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int i;
                    char[] str = s.toString().toCharArray();
                    if (!isDataEdited) {
                        i = DataTimeDevice.formatData(preStr, str, input.getSelectionStart()-1);
                        isDataEdited = true;
                        input.setText(String.valueOf(preStr));
                        input.setSelection(i);
                    } else isDataEdited = false;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            alert.setPositiveButton("Установить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    et_service_datetime.setText(input.getText());
                    // передаем на устройство
                    seqState = SEQ_STATE.LOOP;
                    broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.SET, cmd.DATA_TIME, DataTimeDevice.getCounter(et_service_datetime.getText().toString())));
                }
            });

            alert.setNegativeButton("Омена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            alert.show();
        }
    };

    OnClickListener servicePasswordListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(ContentActivity.this);
            alert.setTitle("             Установка пароля");
            final EditText input = new EditText(ContentActivity.this);
            input.setGravity(Gravity.CENTER);
            input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(10)});
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            alert.setView(input);

            alert.setPositiveButton("Установить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    et_service_password.setText(input.getText());
                    // Send to device
                }
            });

            alert.setNegativeButton("Омена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                }
            });

            alert.show();
        }
    };

    final OnClickListener portsApplayListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // Send data
            // compare mode and newMode, ports and newPorts
            portsControlVisible(false);
            device.newMode.mode = (byte) s_ports_mode.getSelectedItemPosition();
            device.newMode.cntCicles = Byte.parseByte(String.valueOf(tv_ports_cicles.getText()));
            device.newMode.timePeriod.setArrayTime(et_ports_setTimePeriod.getText().toString());
            device.newMode.timeWork.setArrayTime(et_ports_setTimeWork.getText().toString());
            if (device.newMode.timeWork.compareTime(device.newMode.timePeriod) == TimeDevice.RESULT.MORE) device.newMode.timeWork.copyTimeDevice(device.newMode.timePeriod);
            if (!device.mode.compare(device.newMode)) device.edited = true;
            if (device.edited) {                    //

                seqState = SEQ_STATE.SET_CONFIG;
                ll_Wait.setVisibility(View.VISIBLE);
                ll_Info.setVisibility(View.GONE);

                if (device.state.state != 2) {      // STATE != STOP
                    device.sequenceSetConfig = 0;
                    broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.SET, cmd.PAUSE_INV, null));
                } else {                            // STATE == STOP
                    if (!device.mode.compare(device.newMode)) {
                        device.sequenceSetConfig = 1;
                        broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.SET, cmd.TIME_MODE, device.newMode.getMode()));
                    } else {
                        for (byte i = 0; i < 5; i++) {
                            if (device.ports.port[i].compare(device.newPorts.port[i])) {
                                broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.SET, cmd.PIN_MODE, device.newPorts.port[i].getPort(i)));
                                device.sequenceSetConfig = (byte) (i + 2);
                                break;
                            }
                        }
                    }
                }
            }
        }
    };

    OnClickListener portsCancelListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // Return current mode and ports
            device.newMode.copyMode(device.mode);
            device.newPorts.copyPorts(device.ports);
            portsControlVisible(false);
            try {
                showPorts();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };

    OnClickListener portsConfigPortListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            tv_port_in.setText("(Вход " + v.getTag() + ")  Наименование:");

            ll_Ports.setTag(ll_Port);
            ll_old.setVisibility(View.GONE);
            ll_old = ll_Port;
            ll_old.setVisibility(View.VISIBLE);
            device.ports.setSelected(v.getTag());
            try {
                getPortSetFields();
                showPort();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };

    final OnClickListener portApplayListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // Save current port in newPort
            device.newPorts.port[device.ports.numberSelectedPort] = new port();
            try {
                device.newPorts.port[device.ports.numberSelectedPort].setValueName(et_port_name);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            device.newPorts.port[device.ports.numberSelectedPort].typeInput = (byte) s_port_inputType.getSelectedItemPosition();
            device.newPorts.port[device.ports.numberSelectedPort].viewSensor = (byte) s_port_sensor.getSelectedItemPosition();
            device.newPorts.port[device.ports.numberSelectedPort].typeSensor = (byte) s_port_sensorType.getSelectedItemPosition();
            device.newPorts.port[device.ports.numberSelectedPort].normalState = (byte) s_port_sensorBeginState.getSelectedItemPosition();
            device.newPorts.port[device.ports.numberSelectedPort].typeLevelSensor = (byte) s_port_typeLevelSensor.getSelectedItemPosition();
            device.newPorts.port[device.ports.numberSelectedPort].measurementMin.setValue(et_port_measurementMin.getText());
            device.newPorts.port[device.ports.numberSelectedPort].measurementMax.setValue(et_port_rangeMin.getText());
            device.newPorts.port[device.ports.numberSelectedPort].rangeMin.setValue(et_port_rangeMin.getText());
            device.newPorts.port[device.ports.numberSelectedPort].rangeMax.setValue(et_port_rangeMax.getText());
            try {
                device.newPorts.port[device.ports.numberSelectedPort].setValueUnit(et_port_unit);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            device.newPorts.port[device.ports.numberSelectedPort].alarmMin.setValue(et_port_alarmMin.getText());
            device.newPorts.port[device.ports.numberSelectedPort].delayMin.setArrayTime(et_port_timeDelayMin.getText().toString());
            device.newPorts.port[device.ports.numberSelectedPort].reactionMin = (byte) s_port_actionLess.getSelectedItemPosition();
            device.newPorts.port[device.ports.numberSelectedPort].alarmMax.setValue(et_port_alarmMax.getText());
            device.newPorts.port[device.ports.numberSelectedPort].delayMax.setArrayTime(et_port_timeDelayMax.getText().toString());
            device.newPorts.port[device.ports.numberSelectedPort].reactionMax = (byte) s_port_actionMore.getSelectedItemPosition();
            if (!device.ports.port[device.ports.numberSelectedPort].compare(device.newPorts.port[device.ports.numberSelectedPort]))  device.edited = true;
            ll_Ports.setTag(ll_Ports);
            ll_old.setVisibility(View.GONE);
            ll_old = ll_Ports;
            ll_old.setVisibility(View.VISIBLE);
        }
    };

    OnClickListener portCancelListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // Return current Port in port
            device.newPorts.port[device.ports.numberSelectedPort].copyPort(device.ports.port[device.ports.numberSelectedPort]);
            ll_Ports.setTag(ll_Ports);
            ll_old.setVisibility(View.GONE);
            ll_old = ll_Ports;
            ll_old.setVisibility(View.VISIBLE);
        }
    };

    OnClickListener logApplayListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            byte[] bData1 = (device.state.timeDevice.getArrayCounter(et_log_datetime1.getText()));
            byte[] bData2 = (device.state.timeDevice.getArrayCounter(et_log_datetime2.getText()));
            byte[] bData = new byte[8];
            for (byte i=0; i<bData1.length; i++) {
                bData[i] = bData1[i];
            }
            for (byte i=0; i<bData2.length; i++) {
                bData[i+4] = bData2[i];
            }
            //currentCmd = Commands.CMD.SET_EVENT_PERIOD;
            seqState = SEQ_STATE.SET_EVENT_PERIOD;
            broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.SET, cmd.EVENT_RANGE, bData));
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = getCurrentFocus();
            if (view != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    };

    OnClickListener logClearListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            tv_log_out.setText("");
        }
    };

    OnClickListener logNextListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //currentCmd = Commands.CMD.GET_NEXT_EVENT;
            seqState = SEQ_STATE.GET_NEXT_EVENT;
            broadcastUpdate(START_OPERATION, cmd.Constructor(cmd.GET, cmd.NEXT_EVENT, null));
            broadcastUpdate(START_OPERATION, cmd.currentCmd);
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = getCurrentFocus();
            if (view != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    };

    OnClickListener dateTimeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            AlertDialog.Builder alert = new AlertDialog.Builder(ContentActivity.this);
            alert.setTitle("             Установка даты");
            EditText old = (EditText) v;
            EditText input = new EditText(ContentActivity.this);
            input.setGravity(Gravity.CENTER);
            input.setInputType(TYPE_CLASS_DATETIME);
            alert.setView(input);
            input.setText(old.getText());
            input.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    preStr = s.toString().toCharArray();
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int i;
                    char[] str = s.toString().toCharArray();
                    if (!isDataEdited) {
                        isDataEdited = true;
                        i = DataTimeDevice.formatData(preStr, str, input.getSelectionStart()-1);
                        input.setText(String.valueOf(preStr));
                        input.setSelection(i);
                    } else isDataEdited = false;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            alert.setPositiveButton("Установить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    old.setText(input.getText());
                }
            });

            alert.setNegativeButton("Омена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            alert.show();
        }
    };

    OnClickListener timeIntervalListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(ContentActivity.this);
            alert.setTitle("             Установка интервала");
            EditText old = (EditText) v;
            EditText input = new EditText(ContentActivity.this);
            input.setGravity(Gravity.CENTER);
            input.setInputType(TYPE_CLASS_DATETIME);
            alert.setView(input);
            input.setText(old.getText());
            input.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    preStr = s.toString().toCharArray();
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int i;
                    char[] str = s.toString().toCharArray();
                    if (!isDataEdited) {
                        isDataEdited = true;
                        i = TimeDevice.formatTime(preStr, str, input.getSelectionStart()-1);
                        input.setText(String.valueOf(preStr));
                        input.setSelection(i);
                    } else isDataEdited = false;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            alert.setPositiveButton("Установить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    old.setText(input.getText());

                    switch (old.getTag().toString().toCharArray()[0] - 0x30) {
                        case 1: {
                            device.newMode.timeWork.setArrayTime(old.getText().toString());
                            if (device.mode.timeWork.compareTime(device.newMode.timeWork) != TimeDevice.RESULT.EQUALS) {
                                portsControlVisible(true);
                            }
                            break;
                        }
                        case 2: {
                            device.newMode.timePeriod.setArrayTime(old.getText().toString());
                            if (device.mode.timePeriod.compareTime(device.newMode.timePeriod) != TimeDevice.RESULT.EQUALS) {
                                portsControlVisible(true);
                            }
                            break;
                        }
                    }
                }
            });

            alert.setNegativeButton("Омена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            alert.show();
        }
    };

    OnClickListener floatListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(ContentActivity.this);
            alert.setTitle("             Установка значения");
            EditText old = (EditText) v;
            EditText input = new EditText(ContentActivity.this);
            input.setGravity(Gravity.CENTER);
            input.setInputType(TYPE_CLASS_NUMBER | TYPE_NUMBER_FLAG_SIGNED | TYPE_NUMBER_FLAG_DECIMAL);
            input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(7)});
            alert.setView(input);
            input.setText(old.getText());
            alert.setPositiveButton("Установить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    old.setText(input.getText());

                }
            });

            alert.setNegativeButton("Омена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            alert.show();
        }
    };

    OnClickListener integerListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(ContentActivity.this);
            alert.setTitle("             Установка значения");
            EditText old = (EditText) v;
            EditText input = new EditText(ContentActivity.this);
            input.setGravity(Gravity.CENTER);
            input.setInputType(TYPE_CLASS_NUMBER);
            input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(2)});
            alert.setView(input);
            input.setText(old.getText());
            alert.setPositiveButton("Установить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    old.setText(input.getText());

                }
            });

            alert.setNegativeButton("Омена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            alert.show();
        }
    };

    OnClickListener textListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(ContentActivity.this);
            alert.setTitle("             Установка значения");
            EditText old = (EditText) v;
            EditText input = new EditText(ContentActivity.this);
            input.setGravity(Gravity.CENTER);
            input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(old.getMaxLines())});
            alert.setView(input);
            input.setText(old.getText());
            alert.setPositiveButton("Установить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    old.setText(input.getText());

                }
            });

            alert.setNegativeButton("Омена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            alert.show();
        }
    };

    void CorrectEmptyView(AppCompatEditText text) {
        if (text.getText().toString().isEmpty()) {
            text.setText("00");
        }
    }
    /********************VIEWs**********************/
    public void showInfo() throws UnsupportedEncodingException {
        if (device.state.mode != 1)  tr_info_currentCycle.setVisibility(View.GONE);
        else tr_info_currentCycle.setVisibility(View.VISIBLE);

        if (device.ports.port[0].typeInput == 3) tr_info_input1.setVisibility(View.GONE);
        else {
            tr_info_input1.setVisibility(View.VISIBLE);
            tv_info_input1_name.setText(device.ports.port[0].getStringName());
        }
        if (device.ports.port[1].typeInput == 3) tr_info_input2.setVisibility(View.GONE);
        else {
            tr_info_input2.setVisibility(View.VISIBLE);
            tv_info_input2.setText(device.ports.port[1].getStringName());
        }
        if (device.ports.port[2].typeInput == 3) tr_info_input3.setVisibility(View.GONE);
        else {
            tr_info_input3.setVisibility(View.VISIBLE);
            tv_info_input3.setText(device.ports.port[2].getStringName());
        }
        if (device.ports.port[3].typeInput == 3) tr_info_input4.setVisibility(View.GONE);
        else {
            tr_info_input4.setVisibility(View.VISIBLE);
            tv_info_input4_name.setText(device.ports.port[3].getStringName());
        }
    }

    public void showService() {
        et_service_datetime.setText(device.state.timeDevice.getStringDataTime());
    }

    public void showPorts() throws UnsupportedEncodingException {
        s_ports_mode.setSelection(device.mode.mode);
        if (device.mode.mode !=3) tr_ports_cycles.setVisibility(View.GONE);
        else tr_ports_cycles.setVisibility(View.VISIBLE);
        et_ports_setTimeWork.setText(device.mode.timeWork.getStringTime());
        et_ports_setTimePeriod.setText(device.mode.timePeriod.getStringTime());
        tv_ports_input1.setText(device.ports.port[0].getStringName());
        tv_ports_input2.setText(device.ports.port[1].getStringName());
        tv_ports_input3.setText(device.ports.port[2].getStringName());
        tv_ports_input4.setText(device.ports.port[3].getStringName());
        tv_ports_input5.setText(device.ports.port[4].getStringName());
    }

    public void getPortSetFields() throws UnsupportedEncodingException {
        s_port_inputType.setSelection(device.ports.selected.typeInput);
        s_port_sensor.setSelection(device.ports.selected.viewSensor);
        s_port_sensorType.setSelection(device.ports.selected.typeSensor);
        s_port_sensorBeginState.setSelection(device.ports.selected.normalState);
        s_port_typeLevelSensor.setSelection(device.ports.selected.typeLevelSensor);
        et_port_measurementMin.setText(device.ports.selected.measurementMin.getString(2));
        et_port_measurementMax.setText(device.ports.selected.measurementMax.getString(2));
        et_port_rangeMin.setText(device.ports.selected.rangeMin.getString(0));
        et_port_rangeMax.setText(device.ports.selected.rangeMax.getString(0));
        et_port_unit.setText(device.ports.selected.getStringUnit());
        et_port_alarmMin.setText(device.ports.selected.alarmMin.getString(0));
        et_port_timeDelayMin.setText(device.ports.selected.delayMin.getStringTime());
        s_port_actionLess.setSelection(device.ports.selected.reactionMin);
        et_port_alarmMax.setText(device.ports.selected.alarmMax.getString(0));
        et_port_timeDelayMax.setText(device.ports.selected.delayMax.getStringTime());
        s_port_actionMore.setSelection(device.ports.selected.reactionMax);
    }

    public void showPort() throws UnsupportedEncodingException {
        et_port_name.setText(device.ports.selected.getStringName());
        if (s_port_inputType.getSelectedItemPosition() == 3) {
            tr_port_viewSensor.setVisibility(View.GONE);
            tr_port_typeSensor.setVisibility(View.GONE);
            tr_port_normSensor.setVisibility(View.GONE);
            tr_port_typeLevelSensor.setVisibility(View.GONE);
            tr_port_minMeasure.setVisibility(View.GONE);
            tr_port_maxMeasure.setVisibility(View.GONE);
            tr_port_minRange.setVisibility(View.GONE);
            tr_port_maxRange.setVisibility(View.GONE);
            tr_port_unit.setVisibility(View.GONE);
            tr_port_range.setVisibility(View.GONE);
            tr_port_minAlarm.setVisibility(View.GONE);
            tr_port_timeDelayMin.setVisibility(View.GONE);
            tr_port_minAlarmReaction.setVisibility(View.GONE);
            tr_port_maxAlarm.setVisibility(View.GONE);
            tr_port_timeDelayMax.setVisibility(View.GONE);
            tr_port_maxAlarmReaction.setVisibility(View.GONE);
            return;
        }
            tr_port_viewSensor.setVisibility(View.VISIBLE);
            if (s_port_inputType.getSelectedItemPosition() == 0) { // Цифровой
                tr_port_typeSensor.setVisibility(View.VISIBLE);
                tr_port_normSensor.setVisibility(View.VISIBLE);
                if (s_port_sensor.getSelectedItemPosition()== 3) tr_port_typeLevelSensor.setVisibility(View.VISIBLE);
                else tr_port_typeLevelSensor.setVisibility(View.GONE);
                tr_port_minMeasure.setVisibility(View.GONE);
                tr_port_maxMeasure.setVisibility(View.GONE);
                tr_port_minRange.setVisibility(View.GONE);
                tr_port_maxRange.setVisibility(View.GONE);
                tr_port_unit.setVisibility(View.GONE);
                tr_port_range.setVisibility(View.VISIBLE);
                tr_port_minAlarm.setVisibility(View.GONE);
                tr_port_minAlarmReaction.setVisibility(View.VISIBLE);
                tr_port_maxAlarm.setVisibility(View.GONE);
            } else { // !Цифровой
                tr_port_typeSensor.setVisibility(View.GONE);
                tr_port_normSensor.setVisibility(View.GONE);
                tr_port_typeLevelSensor.setVisibility(View.GONE);
                tr_port_minMeasure.setVisibility(View.VISIBLE);
                tr_port_maxMeasure.setVisibility(View.VISIBLE);
                tr_port_minRange.setVisibility(View.VISIBLE);
                tr_port_maxRange.setVisibility(View.VISIBLE);
                tr_port_unit.setVisibility(View.VISIBLE);
                tr_port_range.setVisibility(View.VISIBLE);
                tr_port_minAlarm.setVisibility(View.VISIBLE);
                tr_port_maxAlarm.setVisibility(View.VISIBLE);
            }
        tr_port_minAlarmReaction.setVisibility(View.VISIBLE);
        byte r = (byte)s_port_actionLess.getSelectedItemPosition();
        if (s_port_actionLess.getSelectedItemPosition() ==2) tr_port_timeDelayMin.setVisibility(View.VISIBLE);
        else tr_port_timeDelayMin.setVisibility(View.GONE);
        tr_port_maxAlarmReaction.setVisibility(View.VISIBLE);
        r = (byte)s_port_actionMore.getSelectedItemPosition();
        if (s_port_actionMore.getSelectedItemPosition() ==2) tr_port_timeDelayMax.setVisibility(View.VISIBLE);
        else tr_port_timeDelayMax.setVisibility(View.GONE);
        }

    public void showLog() {
        et_log_datetime1.setText(device.state.timeDevice.getStringDataTimePeriod(true));
        et_log_datetime2.setText(device.state.timeDevice.getStringDataTimePeriod(false));
    }

    public void portsControlVisible(boolean visible) {
        tv_ports_applay.setClickable(visible);
        tv_ports_cancel.setClickable(visible);
        if (visible) {
            tv_ports_applay.setTextColor(Color.GREEN);
            tv_ports_cancel.setTextColor(Color.GREEN);
        } else {
            tv_ports_applay.setTextColor(Color.GRAY);
            tv_ports_cancel.setTextColor(Color.GRAY);
        }
    }
}