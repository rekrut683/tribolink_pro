package pro.tribo.TriboPro;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import pro.tribo.TriboPro.classes.BluetoothConnect;

public class ConnectActivity extends AppCompatActivity implements BluetoothConnect.ConnectCallback {
    BluetoothConnect btConnect;
    String name = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        Intent intent = getIntent();
        String mac = intent.getStringExtra("address");
        name = intent.getStringExtra("name");
        btConnect = new BluetoothConnect(this, mac);
        btConnect.SubscribeCallback(this);
        btConnect.connect();
        Log.e("AAA","Connect start");
    }
    @Override
    public void onBackPressed() {
        if(btConnect!=null) {
            btConnect.disconnect();
        }
        finish();
        return;
    }
    @Override
    public void onConnectCallback(boolean status) {
        String strStatus = "";
        int retValue = 0;
        //Log.e("AAA","connect callback start");
        if(status) {
            strStatus = "Connect OK";
            retValue = -100;
        } else {
            strStatus = "Connect Failed";
            // Должно соединятся без устройства
            //retValue = -101;
            retValue = -100;
        }
        Intent intent = new Intent();
        intent.putExtra("resultConnect", retValue);
        intent.putExtra("name", name);
        MainActivity.btConnect = this.btConnect;
        setResult(RESULT_OK, intent);
        finish();
        //Log.e("AAA","connect callback finish");
    }
    @Override
    public void onDisconnectCallback(boolean status) {
    }

    @Override
    public void onReadCallback(byte[] data) {

    }

}