package pro.tribo.TriboPro.classes;

public class RecyclerItem {
    private int id;
    private String name;
    private String devMac;

    public RecyclerItem(int id, String name, String mac) {
        this.id = id;
        this.name = name;
        this.devMac = mac;
    }

    public int getId(){
        return this.id;
    }
    public String getName()
    {
        return this.name;
    }
    public String getMac() {
        return devMac;
    }

    public void setMac(String mac) {
        this.devMac = mac;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public void setName(String name)
    {
        this.name = name;
    }
}
