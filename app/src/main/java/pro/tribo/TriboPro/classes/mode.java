package pro.tribo.TriboPro.classes;

public class mode {

    public byte mode;
    public byte cntCicles;
    public TimeDevice timePeriod;
    public TimeDevice timeWork;

    public mode(byte[] data) {
        this.mode = data[0];
        this.cntCicles = data[1];
        this.timePeriod = new TimeDevice(data[2],data[3],data[4]);
        this.timeWork = new TimeDevice(data[5],data[6],data[7]);
    }

    public byte [] getMode() {
        byte ret[] = new byte[8];
        ret[0] = this.mode;
        ret[1] = this.cntCicles;
        ret[2] = this.timePeriod.h;
        ret[3] = this.timePeriod.m;
        ret[4] = this.timePeriod.s;
        ret[5] = this.timeWork.h;
        ret[6] = this.timeWork.m;
        ret[7] = this.timeWork.s;
        return ret;
    }

    public mode() {
        this.mode = 0;
        this.cntCicles = 0;
        this.timePeriod = new TimeDevice();
        this.timeWork = new TimeDevice();
    }

    public void copyMode(mode mode) {
        this.mode = mode.mode;
        this.cntCicles = mode.cntCicles;
        this.timePeriod.copyTime(mode.timePeriod);
        this.timeWork.copyTime(mode.timeWork);
    }

    public boolean compare(mode mode) {
        if ((this.mode != mode.mode) ||
                (this.cntCicles != mode.cntCicles) ||
                (!this.timePeriod.compare(mode.timePeriod)) ||
                (!this.timeWork.compare(mode.timeWork))) {
            return false;
        } else return true;
    }
}
