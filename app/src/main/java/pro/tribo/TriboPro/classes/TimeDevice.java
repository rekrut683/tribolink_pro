package pro.tribo.TriboPro.classes;

public class TimeDevice {

    public enum RESULT {
        EQUALS,
        MORE,
        LESS
    }

    public byte h;
    public byte m;
    public byte s;

    public TimeDevice() {
        h = 0;
        m = 0;
        s = 0;
    };

    public TimeDevice(byte h, byte m, byte s) {
        this.h = h;
        if(m > 0x59) m = 0x59;
        this.m = m;
        if(s > 0x59) s = 0x59;
        this.s = s;
    }

    public TimeDevice(byte[] bb) {
        this.h = bb[0];
        if(bb[1] > 0x59) bb[1] = 0x59;
        this.m = bb[1];
        if(bb[2] > 0x59) bb[2] = 0x59;
        this.s = bb[2];
    }

    public boolean compare(TimeDevice t) {
        if(this.h != t.h || this.m != t.m || this.s != t.s) return false;
        return true;
    }

    public void set(TimeDevice t) {
        this.h = t.h;
        this.m = t.m;
        this.s = t.s;
    }

    public RESULT compareTime(TimeDevice b) {
        int timeA = this.h *3600 + this.m *60 + this.s;
        int timeB = b.h *3600 + b.m *60 + b.s;
        if (timeA == timeB) return RESULT.EQUALS;
        if (timeA > timeB) return RESULT.MORE;
        else return RESULT.LESS;
    }

    public void copyTime(TimeDevice t) {
        this.h = t.h;
        this.m = t.m;
        this.s = t.s;
    }

    public String getStringTime() {
        return ("" + (char)((this.h /10)+0x30) + (char)((this.h %10)+0x30) + " : " +
                (char)((this.m /10)+0x30) + (char)((this.m %10)+0x30) + " : " +
                (char)((this.s /10)+0x30) + (char)((this.s %10)+0x30));
    }

    public void setArrayTime(String str) {
        byte[] data = str.getBytes();
        byte h = (byte) ((data[0] - 0x30)*10 +  (data[1] - 0x30));
        byte m = (byte) ((data[5] - 0x30)*10 +  (data[6] - 0x30));
        byte s = (byte) ((data[10] - 0x30)*10 +  (data[11] - 0x30));
        if (h<100 && m<60 && s<60) {
            this.h = h;
            this.m = m;
            this.s = s;
        }
    }

    public String getStringHour() {
        return ("" + (char)((this.h /10)+0x30) + (char)((this.h %10)+0x30));
    }

    public String getStringMinute() {
        return ("" + (char)((this.m /10)+0x30) + (char)((this.m %10)+0x30));
    }

    public String getStringSeconds() {
        return ("" + (char)((this.s /10)+0x30) + (char)((this.s %10)+0x30));
    }

    public void setHours(byte H) {
        if ((H <100) & (H>-1)) this.h = H;
        else this.h = 0;
    }

    public void setMinutes(byte M) {
        if ((M < 60) & (M > -1)) this.m = M;
        else this.m = 0;
    }

    public void setS(byte S) {
        if ((S < 60) & (S > -1)) this.s = S;
        else this.s = 0;
    }

    public void copyTimeDevice(TimeDevice data) {
        this.h = data.h;
        this.m = data.m;
        this.s = data.s;
    }

    public static int formatTime(char[] preStr, char[] str, int sel) {
        int i = 0;
        switch (sel) {
            case 0: {                                                                               // Десятки часы
                if (str[0] >= '0' && str[0] <= '9') {
                    preStr[0] = str[0];
                    i = 1;
                } else i = 0;
                break;
            }
            case 1: {
                if (str[1] >= '0' && str[1] <= '9') {                                               // Еденицы часы
                    preStr[1] = str[1];
                    i = 5;
                } else i = 1;
                break;
            }
            case 2: {
                i = 5;
                if (str[2] != ' ') {                                                                // Разделитель
                    if (str[2] >= '0' && str[2] <= '5') {                                           // Десятки минуты
                        preStr[5] = str[2];
                        i = 6;
                    }
                }
                break;
            }
            case 3: {
                i = 5;
                if (str[3] != ':') {                                                                // Разделитель
                    if (str[3] >= '0' && str[3] <= '5') {                                           // Десятки минуты
                        preStr[5] = str[3];
                        i = 6;
                    }
                }
                break;
            }
            case 4: {
                i = 5;
                if (str[4] != ' ') {                                                                // Разделитель
                    if (str[4] >= '0' && str[4] <= '5') {                                           // Десятки минуты
                        preStr[5] = str[4];
                        i = 6;
                    }
                }
                break;
            }
            case 5: {
                if (str[5] >= '0' && str[5] <= '5') {                                               // Десятки минуты
                    preStr[5] = str[5];
                    i = 6;
                } else i = 5;
                break;
            }
            case 6: {                                                                               // Еденицы минуты
                if (str[6] >= '0' && str[6] <= '9') {
                    preStr[6] = str[6];
                    i = 10;
                } else i = 6;
                break;
            }
            case 7: {
                i = 10;
                if (str[7] != ' ') {                                                                // Разделитель
                    if (str[7] >= '0' && str[7] <= '5') {                                           // Десятки секунды
                        preStr[10] = str[7];
                        i = 11;
                    }
                }
                break;
            }
            case 8: {
                i = 10;
                if (str[8] != ':') {                                                                // Разделитель
                    if (str[8] >= '0' && str[8] <= '5') {                                           // Десятки секунды
                        preStr[10] = str[8];
                        i = 11;
                    }
                }
                break;
            }
            case 9: {
                i = 10;
                if (str[9] != ' ') {                                                                // Разделитель
                    if (str[9] >= '0' && str[9] <= '5') {                                           // Десятки секунды
                        preStr[10] = str[9];
                        i = 11;
                    }
                }
                break;
            }
            case 10: {
                if (str[10] >= '0' && str[10] <= '5') {                                             // Десятки секунды
                    preStr[10] = str[10];
                    i = 11;
                } else i = 10;
                break;
            }
            case 11: {                                                                              // Еденицы секунды
                if (str[11] >= '0' && str[11] <= '9') {
                    preStr[11] = str[11];
                    i = 12;
                } else i = 11;
                break;
            }
        }
        return  i;
    }
}