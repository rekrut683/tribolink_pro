package pro.tribo.TriboPro.classes;

import android.text.Editable;

public class FloatDeviceData {

    public byte integerData;
    public byte fractData;
    public short data;

    public FloatDeviceData() {
        integerData=0;
        fractData=0;
        data = 0;
    };
    public FloatDeviceData(byte i, byte f) {
        this.integerData=i;
        this.fractData=f;
        this.data = (short)((i<<8) + f);
    }

    public String getString(int fract) {          // 0 - (-32768, 32767), 1 - (-3276.8, 3276.7), 2 - (-327.68, 327.67), 3 - (-32.768, 32.767)
        String str;
        if (fract == 0) return String.valueOf(this.integerData);                // 0
        else if (fract == 1) {
            str = String.valueOf(fract / 10);
            str += ".";
            str += String.valueOf(fract % 10);
            return str;                                                         // 1
        } else if (fract == 2) {
            str = String.valueOf(fract / 100);
            str += ".";
            fract %= 100;
            str += String.valueOf(fract);
            str += String.valueOf(fract % 10);
            return str;                                                         // 2
        } else if (fract == 3) {
            str = String.valueOf(fract / 1000);
            str += ".";
            fract %= 1000;
            str += String.valueOf(fract);
            fract %= 100;
            str += String.valueOf(fract % 10);
            return str;                                                         // 3
        } else {
            str = String.valueOf(integerData) + "." + String.valueOf(fractData / 10);
            return  str;
        }
    }

    public void setValue(Editable e) {
        String str;
        String s;
        str = e.toString();
        int i = str.indexOf(".");
        if (i > 0) {
            s = str.substring(0, i);
            data = Short.valueOf(s);
            i++;
        } else { data = 0; i = 0;}                                               // not dot
        s = str.substring(i, str.length());
        data += Short.valueOf(s);
        this.fractData = (byte) this.data;
        this.integerData = (byte) (this.data>>8);
    }

    public void set(byte i, byte f){
        this.integerData = i;
        this.fractData = f;
        this.data = (short)((i<<8) + f);
    }

    public void copyFloatDeviceData(FloatDeviceData data) {
        this.integerData = data.integerData;
        this.fractData = data.fractData;
        this.data = data.data;
    }
}