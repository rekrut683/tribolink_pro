package pro.tribo.TriboPro.classes;

import pro.tribo.TriboPro.ContentActivity;

public class Commands {
    /******************CMD********************/
    public final byte GET = 1;
    public final byte SET = 2;
    public final byte ACTION = 3;
    /***************CMD_EXTRA*****************/
    public final byte VERSION = 1;      //GET
    public final byte PAUSE_INV = 1;    //SET
    public final byte HELLO = 1;        //ACTION
    public final byte TIME_MODE = 2;    //GET SET
    public final byte BYE = 2;          //ACTION
    public final byte STATE = 3;        //GET
    public final byte ADD_WORK = 3;     //SET
    public final byte PING = 3;         //ACTION
    public final byte PIN_MODE = 4;     //GET SET
    public final byte DATA_TIME = 5;    //GET SET
    public final byte NEXT_EVENT = 6;   //GET
    public final byte EVENT_RANGE = 6;  //SET
    public final byte CLEAR_W25Q = 7;   //SET

    public byte[] currentCmd;

    public Package Package = new Package();

    public Commands() {
    }

    public byte[] Constructor(byte cmd, byte extra, byte[] Data) {
        byte[] ret;
        if (Data == null) ret = new byte[2];
        else {
            ret = new byte[Data.length + 2];
            for (byte i = 0; i < Data.length; i++) {
                ret[i + 2] = Data[i];
            }
        }
        ret[0] = cmd;
        ret[1] = extra;
        return Package.packageBuilder(ret);
    }

    public boolean getLogEnd() {
        if (currentCmd[2] == 0) return false;
        else return true;
    }

    public boolean getResponseAddWork() {
        if (currentCmd[2] == 0) return false;
        else return true;
    }

    public boolean getResponseStartStop() {
        if (currentCmd[2] == 0) return false;
        else return true;
    }

    public byte[] getCurrentData() {
        byte[] ret = new byte[currentCmd.length - 2];
        System.arraycopy(currentCmd, 2, ret, 0, ret.length);
        return ret;
    }
}