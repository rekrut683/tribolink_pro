package pro.tribo.TriboPro.classes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import pro.tribo.TriboPro.R;

public class DeviceList extends RecyclerView.Adapter<DeviceList.ViewHolder> {

    public interface DeviceBind{
        void onBind(ViewHolder holder);
    }

    private List<RecyclerItem> listItems;
    private Context mContext;
    private ArrayList<DeviceBind> callbacks;

    public void SubscribeCallbacks(DeviceBind binder){
        callbacks.add(binder);
    }

    public DeviceList(){
        this.listItems = new ArrayList<>();
        this.mContext = mContext;
        callbacks = new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public int id;
        public TextView txtMac;
        public ViewHolder(View itemView) {
            super(itemView);
            txtMac = (TextView) itemView.findViewById(R.id.boardName);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.elem_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        for(DeviceBind clbk : callbacks){
            clbk.onBind(holder);
        }
    }


    @Override
    public int getItemCount() {
        return listItems.size();
    }
}
