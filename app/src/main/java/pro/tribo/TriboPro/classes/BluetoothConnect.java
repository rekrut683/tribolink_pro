package pro.tribo.TriboPro.classes;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import pro.tribo.TriboPro.ContentActivity;

public class BluetoothConnect {

    public Package Package = new Package();
    // private local fields
    private byte[] dataLong = new byte[40];
    private boolean tockenLongPacket = false;
    private String address = "";
    private boolean mScanning;
    private Handler mHandler;
    private boolean mStatusConnect = false;
    private Context mContext;
    // settings
    private int SCAN_PERIOD = 2000; // вернуть на 20000
    // callbacks
    private ArrayList<ConnectCallback> connCallbacks;
    // Bluetooth Adapter
    private BluetoothAdapter bluetoothAdapter;
    // Bluetooth Gatt Server
    private BluetoothDevice btDevice;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private BluetoothGatt bluetoothGatt;
    private int connectionState = STATE_DISCONNECTED;
    // Bluetooth Gatt Constants
    private final static UUID UUID_NOTIFY =
            UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
    private final static UUID UUID_SERVICE =
            UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    private final static UUID CLIENT_UUID =
            UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public interface ConnectCallback {
        void onConnectCallback(boolean status);
        void onDisconnectCallback(boolean status);
        void onReadCallback(byte[] data);
    }

    public BluetoothConnect(Context context, String mac) {
        this.mContext = context;
        this.address = mac;
        mScanning = false;
        mHandler = new Handler();
        BluetoothManager btManager = (BluetoothManager)mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = btManager.getAdapter();
        connCallbacks = new ArrayList<>();
    }

    public boolean connect() {
        //this.connCallback = callback;
        scanLeDevice(mScanning);
        return true;
    }

    public boolean disconnect() {
        if(bluetoothGatt != null && btDevice != null) {
            bluetoothGatt.disconnect();
            bluetoothGatt.close();
            //mNotifyCharacteristic.
        }
        return true;
    }

    private void scanLeDevice(final boolean enable) {
        if (!mScanning) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    //Log.d("TT","stopScan");
                    //rv.setVisibility(View.VISIBLE);
                    if(bluetoothAdapter != null) {
                        //bluetoothAdapter.stopLeScan(leScanCallback);
                        stopScan();
                        for(ConnectCallback clbk : connCallbacks)
                        {
                            clbk.onConnectCallback(false);
                        }
                        //connCallback.onConnectCallback(false);
                    }
                }
            }, SCAN_PERIOD);
            mScanning = true;
            //Log.d("TT","startSCan");
            if(bluetoothAdapter != null) {
                bluetoothAdapter.startLeScan(leScanCallback);
            }
        } else {
            mScanning = false;
            //Log.d("TT","stopScan");
            if(bluetoothAdapter != null) {
                //bluetoothAdapter.stopLeScan(leScanCallback);
                stopScan();
            }
        }
    }

    private void stopScan() {
        if(mScanning || !mScanning) {
            if(bluetoothAdapter !=null && bluetoothAdapter.isEnabled()){
                bluetoothAdapter.stopLeScan(leScanCallback);
            }
        }
        //mScanning = false;
        //mHandler.removeMessages(0);
        //Log.e("ff","bt callback");
        //this.connCallback.onConnectCallback(mStatusConnect);
    }
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback leScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                    ((Activity)mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Log.d("TT","dev: "+device.getName()+", mac: "+device.getAddress());
                            //Log.d("TTT", address);
                            if(device.getAddress().equals(address))
                            {
                                //mStatusConnect = true;
                                stopScan();
                                btDevice = device;
                                bluetoothGatt = btDevice.connectGatt(mContext,true, gattCallback);
                            }
                            mHandler.sendEmptyMessage(1);
                        }
                    });
                }
            };

    public boolean isSend = false;
    public void WriteBytes(byte[] data) {

        if(mNotifyCharacteristic == null)
        {
            Log.e("Tribo2","char is null");
            return;
        }

        mNotifyCharacteristic.setValue(data);

        final byte[] dataWrite = mNotifyCharacteristic.getValue();
        final StringBuilder stringBuilder = new StringBuilder(dataWrite.length);
        for(byte byteChar : dataWrite)
            stringBuilder.append(String.format("%02X ", byteChar));
        Log.e("Tribo2WRITE", stringBuilder.toString());

        bluetoothGatt.writeCharacteristic(mNotifyCharacteristic);
        isSend = false;
    }

    public void SubscribeCallback(ConnectCallback dCallback)
    {
        connCallbacks.add(dCallback);
    }

    // Device GATT Callback
    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //Log.e("R","Read characteristic");
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            bluetoothGatt.readCharacteristic(mNotifyCharacteristic);
            // текущий принятый пакет
            if (!Package.packageCollector(characteristic.getValue())) return;
            Log.e("Tribo2Read","read data:"+ Arrays.toString(Package.packageBuilder(Package.dataLong)));
            for(ConnectCallback clbk : connCallbacks) {
                clbk.onReadCallback(Package.dataLong);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic,
                                          int status) {
            //Log.e("W", "OnCharacteristicWrite");
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt,
                                     BluetoothGattDescriptor bd,
                                     int status) {
            //Log.e("R", "onDescriptorRead");
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt,
                                      BluetoothGattDescriptor bd,
                                      int status) {
            //Log.e("W", "onDescriptorWrite");
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int a, int b) {
            //Log.e("R", "onReadRemoteRssi");
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int a) {
            //Log.e("W", "onReliableWriteCompleted");
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.e("Tribo2", "service discovered");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //Log.e("SUCCESS", "onServicesDiscovered received: " + status);
                List<BluetoothGattService> services = gatt.getServices();
                //Log.i("onServicesDiscovered", services.toString());
                Log.e("Tribo2","count services: "+services.size());
                for(BluetoothGattService bService : services) {
                    if(bService.getUuid().toString().equalsIgnoreCase(UUID_SERVICE.toString())) {
                        List<BluetoothGattCharacteristic> gattCharacteristics =
                                bService.getCharacteristics();
                        Log.i("Tribo2", "Count is:" + gattCharacteristics.size());
                        for (BluetoothGattCharacteristic gattCharacteristic :
                                gattCharacteristics) {
                            if(gattCharacteristic.getUuid().toString().equalsIgnoreCase(UUID_NOTIFY.toString())) {
                                Log.e("Tribo2","set characteristics");
                                mNotifyCharacteristic = gattCharacteristic;
                                List<BluetoothGattDescriptor> descriptors = gattCharacteristic.getDescriptors();
                                for(BluetoothGattDescriptor desc : descriptors){
                                    //Log.e("DESC","Descr: "+desc.toString());
                                    if(desc.getUuid().toString().equalsIgnoreCase(CLIENT_UUID.toString())) {
                                        //Log.e("DESC","URRA");
                                        BluetoothGattDescriptor descriptor = gattCharacteristic.getDescriptor(CLIENT_UUID);
                                        byte[] data = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
                                        descriptor.setValue(data);
                                        gatt.writeDescriptor(descriptor);
                                    }
                                }
                                bluetoothGatt.setCharacteristicNotification(gattCharacteristic, true);
                                //bluetoothGatt.readCharacteristic(mNotifyCharacteristic);
                                mHandler.removeMessages(0);
                                for(ConnectCallback clbk : connCallbacks) {
                                    clbk.onConnectCallback(true);
                                }
                                //connCallback.onConnectCallback(true);
                                return;
                            }
                        }
                    }
                }
            } else {
                if(bluetoothGatt.getDevice().getUuids() == null){}
                    //Log.e("GETUUIDS", "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                //Log.e("CONNECT","");
                gatt.discoverServices();
            }
            else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                //Log.e("DISCONNECT","");
                for(ConnectCallback clbk : connCallbacks) {
                    clbk.onDisconnectCallback(true);
                }
                //connCallback.onDisconnectCallback(true);
            }
        }
    };
}

