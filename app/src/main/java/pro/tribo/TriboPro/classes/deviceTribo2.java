package pro.tribo.TriboPro.classes;

import android.view.View;

import java.io.UnsupportedEncodingException;

import pro.tribo.TriboPro.R;

public class deviceTribo2 {

    public static boolean edited;
    public String version;
    public state state;
    public state newState;
    public ports ports;
    public ports newPorts;
    public mode mode;
    public mode newMode;
    public byte sequenceSetConfig;

    public deviceTribo2() {
        edited = false;
        state = new state();
        newState = new state();
        ports = new ports();
        newPorts = new ports();
        mode = new mode();
        newMode = new mode();
    }

    public static class state {

        public byte mode;
        public byte state;
        public byte warningCode;
        public byte alarmCode;
        public TimeDevice timeRemain;
        public byte actualCycle;
        public DataTimeDevice timeDevice;
        public FloatDeviceData input1;
        public FloatDeviceData input2;
        public FloatDeviceData input3;
        public FloatDeviceData input4;
        public FloatDeviceData input5;
        public FloatDeviceData current;
        public FloatDeviceData voltageIn;
        public FloatDeviceData voltageOut;

        state() {
            this.mode = 0;
            this.state = 0;
            this.warningCode = 0;
            this.alarmCode = 0;
            this.timeRemain = new TimeDevice();
            this.actualCycle = 0;
            this.timeDevice = new DataTimeDevice();
            this.input1 = new FloatDeviceData();
            this.input2 = new FloatDeviceData();
            this.input3 = new FloatDeviceData();
            this.input4 = new FloatDeviceData();
            this.input5 = new FloatDeviceData();
            this.current = new FloatDeviceData();
            this.voltageIn = new FloatDeviceData();
            this.voltageOut = new FloatDeviceData();
        }

        public state(byte[] data) {
            this.mode = data[2];
            this.state = data[3];
            this.warningCode = data[4];
            this.alarmCode = data[5];
            this.timeRemain = new TimeDevice(data[6], data[7], data[8]);
            this.actualCycle = data[9];
            this.timeDevice = new DataTimeDevice(data[13], data[12], data[11], data[10]);
            this.input1 = new FloatDeviceData(data[15], data[14]);
            this.input2 = new FloatDeviceData(data[17], data[16]);
            this.input3 = new FloatDeviceData(data[19], data[18]);
            this.input4 = new FloatDeviceData(data[21], data[20]);
            this.current = new FloatDeviceData(data[23], data[22]);
            this.voltageIn = new FloatDeviceData(data[25], data[24]);
            this.voltageOut = new FloatDeviceData(data[27], data[26]);
        }

        public String getStringMode() {
            String ret = String.valueOf(R.string.ERROR);
            if (this.mode == 0) ret = String.valueOf(R.string.TIMER);
            else if (this.mode == 1) ret = String.valueOf(R.string.CICLE);
            else if (this.mode == 2) ret = String.valueOf(R.string.PRESSURE);
            return ret;
        }

        public String getStringState() {
            String ret = String.valueOf(R.string.ERROR);
            if (this.state == 0) ret = String.valueOf(R.string.WAIT);
            else if (this.state == 1) ret = String.valueOf(R.string.WORK);
            else if (this.state == 2) ret = String.valueOf(R.string.PAUSE);
            else if (this.state == 3) ret = String.valueOf(R.string.ALARM);
            return ret;
        }

        public String getStringWarningCode() {
            String ret = String.valueOf(R.string.ERROR);
            if (this.warningCode == 0) ret = "";
            else if((this.warningCode & (byte)0x1) != 0) ret += "T";
            if((this.warningCode & (byte)0x2) != 0) ret += "L";
            return ret;
        }

        public String getStringAlarmCode() {
            String ret = String.valueOf(R.string.ERROR);
            if (this.alarmCode == 0) ret = "";
            else if ((this.alarmCode & (byte)0x1) != 0) ret += "P";
            else if ((this.alarmCode & (byte)0x2) != 0) ret += "C";
            else if ((this.alarmCode & (byte)0x4) != 0) ret += "L";
            else if ((this.alarmCode & (byte)0x8) != 0) ret += "I";
            return ret;
        }

    }

    public class ports {
        public port[] port = new port[5];
        public port selected;
        public byte seqCounter;
        public byte numberSelectedPort;

        public ports() {
            for (byte i = 0; i < this.port.length; i++) {
               this.port[i] = new port();
            }
        }

        public void setSelected(Object port) {
            String str = (String) port;
            byte[] sel = str.getBytes();
            this.numberSelectedPort = (byte)(sel[0]-0x30-1);
            this.selected = this.port[this.numberSelectedPort];
        }

        public  byte  setInput(byte[] data) {
            if (data[2] >=0 && data[2] <5){
               this.port[data[2]] = new port(data);

            }
            return data[2];
        }

        public void copyPorts(ports data) {
            for (int i=0; i<5; i++) {
               this.port[i].copyPort(data.port[i]);
            }
        }
    }
}
