package pro.tribo.TriboPro.classes;

import android.text.Editable;

import static java.lang.Long.toUnsignedString;

public class DataTimeDevice {

    public Long counter;
    public byte day;
    public byte month;
    public short year;
    public byte hours;
    public byte minutes;
    public byte seconds;

    public DataTimeDevice() {
        day = 0;
        month = 0;
        year = 0;
        hours = 0;
        minutes = 0;
        seconds = 0;
        counter = Long.valueOf(0);
    }
    public DataTimeDevice(byte a, byte b, byte c, byte d) {
        day = 0;
        month = 0;
        year = 0;
        hours = 0;
        minutes = 0;
        seconds = 0;
        counter = 0xFF000000L & (a << 24);
        counter |= (0x00FF0000L & (b << 16));
        counter |= (0x0000FF00L & (c << 8));
        counter |= (0x000000FFL & d);
        setCounter(counter);
    }

    public void set(DataTimeDevice dt) {
        setCounter(dt.counter);
    }

    public void setCounter(long counter) {
        this.counter = counter;
        long a, b, c, d, e, m;
        long jdn = 0;
        jdn = (((counter+43200)/(86400>>1)) + (2440587<<1) + 1)>>1;
        this.seconds =  (byte) (counter % 60);
        counter /= 60;
        this.minutes = (byte) (counter % 60);
        counter /= 60;
        this.hours = (byte)(counter % 24);
        a = jdn + 32044;
        b = (4*a+3)/146097;
        c = a - (146097*b)/4;
        d = (4*c+3)/1461;
        e = c - (1461*d)/4;
        m = (5*e+2)/153;
        this.day = (byte) (e - (153*m+2)/5 + 1);
        this.month = (byte) (m + 3 - 12*(m/10));
        this.year = (short) (100*b + d - 4800 + (m/10));
    }

    public static byte[] getCounter(String str) {
        // Преобразовываем дату в массив из 4 байт
        byte data = (byte) (((str.charAt(0) - 0x30) * 10) + (str.charAt(1) - 0x30));
        byte month = (byte) (((str.charAt(3) - 0x30) * 10) + (str.charAt(4) - 0x30));
        int year = ((str.charAt(6) - 0x30) * 1000 + (str.charAt(7) - 0x30) * 100 + (str.charAt(8) - 0x30) * 10 + (str.charAt(9) - 0x30));
        byte hour = (byte) (((str.charAt(13) - 0x30) * 10) + (str.charAt(14) - 0x30));
        byte min = (byte) (((str.charAt(16) - 0x30) * 10) + (str.charAt(17) - 0x30));
        byte sec = (byte) (((str.charAt(19) - 0x30) * 10) + (str.charAt(20) - 0x30));

        byte a;
        short y;
        byte m;
        long JDN;
        a = (byte) ((14-month)/12);
        y = (short) (year+4800-a);
        m = (byte) (month+(12*a)-3);
        JDN = data;
        JDN += (153 * m + 2) / 5;
        JDN += 365 * y;
        JDN += y / 4;
        JDN += -y/ 100;
        JDN += y/ 400;
        JDN = JDN - 32045;
        JDN = JDN - 2440588;
        JDN *= 86400;
        JDN += (hour*3600);
        JDN += (min*60);
        JDN += sec;

        byte [] d = new byte[4];
        d[0] = (byte) (JDN >> 24);
        d[1] = (byte) (JDN >> 16);
        d[2] = (byte) (JDN >> 8);
        d[3] = (byte) JDN;

        return d;
    }

    public boolean compare(DataTimeDevice dt) {
        return this.counter == dt.counter;
    }

    public String getStringDataTime() {
        return ""+getStringDay()+"."+getStringMonth()+"."+getStringYear()+" / "+getStringHours()+":"+getStringMinutes()+":"+getStringSeconds();
    }

    public String getStringDataTimePeriod(boolean beginPeriod) {
        String retData = ""+getStringDay()+"."+getStringMonth()+"."+getStringYear()+" / ";
        String retTime;
        if (beginPeriod) {
            retTime = "00:00:00";
        } else {
            retTime = "23:59:59";
        }
        return retData + retTime;
    }

    public String getStringDay() {
        return ("" + (char)((this.day/10)+0x30) + (char)((this.day%10)+0x30));
    }

    public String getStringMonth() {
        return ("" + (char)((this.month/10)+0x30) + (char)((this.month%10)+0x30));
    }

    public String getStringYear() {
        short year = this.year;
        String Year = "";
        Year += (char)(year/1000+0x30);
        year %=1000;
        Year += (char)(year/100+0x30);
        year %=100;
        Year += (char)(year/10+0x30);
        year %=10;
        Year +=(char)(year+0x30);
        return Year;
    }

    public String getStringHours() {
        return ("" + (char)((this.hours/10)+0x30) + (char)((this.hours%10)+0x30));
    }

    public String getStringMinutes() {
        return ("" + (char)((this.minutes/10)+0x30) + (char)((this.minutes%10)+0x30));
    }

    public String getStringSeconds() {
        return ("" + (char)((this.seconds/10)+0x30) + (char)((this.seconds%10)+0x30));
    }

    public byte[] getArrayCounter(Editable dateTime) {

        byte day, month, har, minutes, seconds;
        short a, m, year;
        int y;
        long JDN;

        char[] str = new char[21];

        dateTime.getChars(0,21, str,0);
        day = (byte) ((str[0] - 0x30) * 10);
        day += (byte) (str[1] - 0x30);
        if (day > 31) day = 31;
        else if (day < 1) day = 1;
        month = (byte) ((str[3] - 0x30) * 10);
        month += (byte) (str[4] - 0x30);
        if (month > 12) month = 12;
        else if (month < 1) month = 1;
        year = (short) ((str[6] - 0x30) * 1000);
        year += (str[7] - 0x30) * 100;
        year += (str[8] - 0x30) * 10;
        year += (str[9] - 0x30);
        if (year < 0) year = 1970;
        har = (byte) ((str[13] - 0x30) * 10);
        har += (byte) (str[14] - 0x30);
        if (har > 23) har = 23;
        else if (har < 0) har = 0;
        minutes = (byte) ((str[16] - 0x30) * 10);
        minutes += (byte) (str[17] - 0x30);
        if (minutes > 59) minutes = 59;
        else if (minutes < 0) minutes = 0;
        seconds = (byte) ((str[19] - 0x30) * 10);
        seconds += (byte) (str[20] - 0x30);
        if (seconds > 59) seconds = 59;
        else if (seconds < 0) seconds = 0;

        a = (short) ((14 - month) / 12);
        y = year + 4800 - a;
        m = (short) (month + (12 * a) - 3);

        JDN = day;
        JDN += (153 * m + 2) / 5;
        JDN += 365 * y;
        JDN += y / 4;
        JDN += -y / 100;
        JDN += y / 400;
        JDN = JDN - 32045;
        JDN = JDN - 2440588;
        JDN *= 86400;
        JDN += (har * 3600);
        JDN += (minutes * 60);
        JDN += (seconds);

        byte[] ret = new byte[4];
        ret[0] = (byte) (JDN >> 24);
        ret[1] = (byte) (JDN >> 16);
        ret[2] = (byte) (JDN >> 8);
        ret[3] = (byte) JDN;

        return ret;
    }

    public static int formatData(char[] preStr, char[] str, int sel) {
        int i = 0;
        switch (sel) {
            case 0: {
                if (str[0] >= '0' && str[0] <= '3') {                                   // Деятки день
                    preStr[0] = str[0];
                    i = 1;
                }
                break;
            }
            case 1: {
                if (str[0] == '3') {
                    if (str[1] >= '0' && str[1] <= '1') {
                        preStr[1] = str[1];
                        i = 3;
                    } else i = 1;
                } else if (str[1] >= '0' && str[1] <= '9') {                                        // Еденицы день
                    preStr[1] = str[1];
                    i = 3;
                } else i = 1;
                break;
            }
            case 2: {
                i = 3;
                if (str[2] != '.') {                                                                // Разделитель
                    if (str[2] >= '0' && str[2] <= '1') {
                        preStr[3] = str[2];
                        i = 4;
                    }
                }
                break;
            }
            case 3: {
                if (str[3] >= '0' && str[3] <= '1') {                                               // Десятки месяц
                    preStr[3] = str[3];
                    i = 4;
                } else i = 3;
                break;
            }
            case 4: {
                if (str[3] == '1') {
                    if (str[4] >= '0' && str[4] <= '2') {
                        preStr[4] = str[4];
                        i = 6;
                    } else i = 4;
                } else if (str[4] >= '0' && str[4] <= '9') {                                        // Еденицы месяц
                    preStr[4] = str[4];
                    i = 6;
                } else i = 4;
                break;
            }
            case 5: {
                i = 6;
                if (str[5] != '.') {                                                    // Разделитель
                    if (str[5] >= '0' && str[5] <= '9') {
                        preStr[6] = str[5];                                             // Тысячи год
                        i = 7;
                    }
                }
                break;
            }
            case 6: {
                if (str[6] >= '0' && str[6] <= '9') {                                   // Тысячи год
                    preStr[6] = str[6];
                    i = 7;
                } else i = 6;
                break;
            }
            case 7: {
                if (str[7] >= '0' && str[7] <= '9') {                                   // Сотни год
                    preStr[7] = str[7];
                    i = 8;
                } else i = 7;
                break;
            }
            case 8: {
                if (str[8] >= '0' && str[8] <= '9') {                                   // Десятки год
                    preStr[8] = str[8];
                    i = 9;
                } else i = 8;
                break;
            }
            case 9: {                                                                               // Еденицы год
                if (str[9] >= '0' && str[9] <= '9') {
                    preStr[9] = str[9];
                    i = 13;
                } else i = 9;
                break;
            }
            case 10: {
                i = 13;
                if (str[10] != ' ') {                                                               // Разделитель
                    if (str[10] >= '0' && str[10] <= '2') {                                         // Десятки часы
                        preStr[13] = str[10];
                        i = 14;
                    }
                }
                break;
            }
            case 11: {
                i = 13;
                if (str[11] != '/') {
                    if (str[11] >= '0' && str[11] <= '2') {
                        preStr[13] = str[11];
                        i = 14;
                    }
                }
                break;
            }
            case 12: {
                i = 13;
                if (str[12] != ' ') {
                    if (str[12] >= '0' && str[12] <= '2') {
                        preStr[13] = str[12];
                        i = 14;
                    }
                }
                break;
            }
            case 13: {                                                                              // Десятки часы
                if (str[13] >= '0' && str[13] <= '2') {
                    preStr[13] = str[13];
                    i = 14;
                } else i = 13;
                break;
            }
            case 14: {
                if (str[13] == '2') {                                                                             // Еденицы часы
                    if (str[14] >= '0' && str[14] <= '3') {
                        preStr[14] = str[14];
                        i = 16;
                    } else i = 14;
                } else if (str[14] >= '0' && str[14] <= '9') {
                    preStr[14] = str[14];
                    i = 16;
                } else i = 14;
                break;
            }
            case 15: {
                i = 16;
                if (str[15] != ':') {                                                   // Разделитель
                    if (str[15] >= '0' && str[15] <= '5') {                             // Десятки минуты
                        preStr[16] = str[15];
                        i = 17;
                    }
                }
                break;
            }
            case 16: {
                if (str[16] >= '0' && str[16] <= '5') {                                 // Десятки минуты
                    preStr[16] = str[16];
                    i = 17;
                } else i = 16;
                break;
            }
            case 17: {                                                                  // Еденицы минуты
                if (str[17] >= '0' && str[17] <= '9') {
                    preStr[17] = str[17];
                    i = 19;
                } else i = 17;
                break;
            }
            case 18: {
                i = 19;
                if (str[18] != ':') {                                                   // Разделитель
                    if (str[18] >= '0' && str[18] <= '5') {                             // Десятки секунды
                        preStr[19] = str[18];
                        i = 20;
                    }
                }
                break;
            }
            case 19: {
                if (str[19] >= '0' && str[19] <= '5') {                                 // Десятки секунды
                    preStr[19] = str[19];
                    i = 20;
                } else i = 19;
                break;
            }
            case 20: {                                                                  // Еденицы секунды
                if (str[20] >= '0' && str[20] <= '9') {
                    preStr[20] = str[20];
                    i = 21;
                } else i = 20;
                break;
            }
        }
        return  i;
    }


}
