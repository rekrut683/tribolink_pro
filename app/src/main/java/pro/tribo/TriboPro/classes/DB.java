package pro.tribo.TriboPro.classes;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class DB {

    private final String DBName = "appTribo.db";
    private final String TableName = "devices1";
    private Context mContext;
    private SQLiteDatabase db;

    public class DeviceTable{
        public int id;
        public String name;
        public String mac;
    }

    public DB(Context c){
        this.mContext = c;
        open();
        db.execSQL("CREATE TABLE IF NOT EXISTS '"+TableName+"' (devId INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, mac TEXT)");
        db.close();
    }

    private void open(){
        db = mContext.openOrCreateDatabase(DBName, Context.MODE_PRIVATE, null);
    }

    public ArrayList<DeviceTable> getAllRaws(){
        return null;
    }

    public boolean containsMac(String mac){
        return false;
    }

    public void updateNameById(int id, String newName){
        open();
        db.execSQL("UPDATE devices1 SET name = '"+newName+"' WHERE devId="+id);
        db.close();
    }

    public boolean containsName(String name){
        open();
        boolean isFind = false;
        Cursor query = db.rawQuery("SELECT name,mac FROM '"+TableName+"';", null);
        while (query.moveToNext()) {
            if (query.getString(0).equals(name)) {
                isFind = true;
            }
        }
        query.close();
        db.close();
        return isFind;
    }

    public String getMacFromName(String name){
        open();
        Cursor cursor = db.rawQuery("SELECT mac FROM devices1 WHERE name = '"+name+"'",null);
        String mac = null;
        if(cursor.moveToFirst()) {
            mac = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return mac;
    }

    public void deleteDeviceById(int id){
        open();
        db.execSQL("DELETE FROM devices1 WHERE devId = "+id);
        db.close();
    }

}
