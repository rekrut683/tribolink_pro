package pro.tribo.TriboPro.classes;

import android.text.Editable;
import android.widget.EditText;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class port{

    public byte[] name;
    public byte typeInput;
    public byte viewSensor;
    public byte typeSensor;
    public byte normalState;
    public byte typeLevelSensor;
    public FloatDeviceData measurementMin;
    public FloatDeviceData measurementMax;
    public FloatDeviceData rangeMin;
    public FloatDeviceData rangeMax;
    public byte[] unit;
    public FloatDeviceData alarmMin;
    public byte reactionMin;
    public TimeDevice delayMin;
    public FloatDeviceData alarmMax;
    public byte reactionMax;
    public TimeDevice delayMax;

    public port() {
        this.name = new byte[15];
        this.typeInput = 4;
        this.viewSensor = 0;
        this.typeSensor = 0;
        this.normalState = 0;
        this.typeLevelSensor = 0;
        this.measurementMin = new FloatDeviceData();
        this.measurementMax = new FloatDeviceData();
        this.rangeMin = new FloatDeviceData();
        this.rangeMax = new FloatDeviceData();
        this.unit = new byte[5];
        this.alarmMin = new FloatDeviceData();
        this.reactionMin = 0;
        this.delayMin = new TimeDevice();
        this.alarmMax = new FloatDeviceData();
        this.reactionMax = 0;
        this.delayMax = new TimeDevice();
        // length 33
    }

    public  port(byte[] data) {
        this.name = new byte[15];
        System.arraycopy(data,1, this.name, 0,15);
        this.typeInput = data[16];
        this.viewSensor = data[17];
        this.typeSensor = data[18];
        this.normalState = data[19];
        this.typeLevelSensor = data[20];
        this.measurementMin = new FloatDeviceData(data[22], data[21]);
        this.measurementMax = new FloatDeviceData(data[24], data[23]);
        this.rangeMin = new FloatDeviceData(data[26], data[25]);
        this.rangeMax = new FloatDeviceData(data[28], data[27]);
        this.unit = new byte[5];
        System.arraycopy(data,29, this.unit, 0,5);
        this.alarmMin = new FloatDeviceData(data[35], data[34]);
        this.reactionMin = data[36];
        this.delayMin = new TimeDevice(data[37], data[38], data[39]);
        this.alarmMax = new FloatDeviceData(data[41], data[40]);
        this.reactionMax = data[42];
        this.delayMax = new TimeDevice(data[43], data[44], data[45]);
    }

    public static byte[] charToByte(char[] str, int len) {
        byte[] ret = new byte[len];
        for (int i = 0; i < len; i++) {
            ret[i] = (byte) str[i];
        }
        return ret;
    }

    public String getStringName () throws UnsupportedEncodingException {
        if (this.typeInput ==3)  return "Не настроено";
        if (this.name != null) return new String(this.name, "cp1251");
        else return "Не настроено";
    }

    public void setValueName(EditText e) throws UnsupportedEncodingException {
        byte[] ar = e.getText().toString().getBytes("cp1251");
        System.arraycopy(ar, 0,this.name,(this.name.length - ar.length), ar.length);
    }

    public void setValueUnit(EditText e) throws UnsupportedEncodingException {
        byte[] ar = e.getText().toString().getBytes("cp1251");
        System.arraycopy(ar, 0,this.unit,(this.unit.length - ar.length), ar.length);
    }

    public void setStringName (String str) {
        this.name = str.getBytes();
    }

    public String getStringUnit () throws UnsupportedEncodingException {
        if (this.typeInput ==3)  return "";
        String str;
        if (this.unit != null) {
            str = new String(this.unit, "cp1251");
        } else return "";
        return str;
    }

    public void setStringUnit (String str) {
        this.unit = str.getBytes();
    }

    public void copyPort(port port) {
        System.arraycopy(port.name,0,this.name,0,this.name.length);
        this.typeInput = port.typeInput;
        this.viewSensor = port.viewSensor;
        this.typeSensor = port.typeSensor;
        this.normalState = port.normalState;
        this.typeLevelSensor = port.typeLevelSensor;
        this.measurementMin.copyFloatDeviceData(port.measurementMin);
        this.measurementMax.copyFloatDeviceData(port.measurementMax);
        this.rangeMin.copyFloatDeviceData(port.rangeMin);
        this.rangeMax.copyFloatDeviceData(port.rangeMax);
        System.arraycopy(port.unit,0,this.unit,0,this.unit.length);
        this.alarmMin.copyFloatDeviceData(port.alarmMin);
        this.reactionMin = port.reactionMin;
        this.delayMin.copyTimeDevice(port.delayMin);
        this.alarmMax.copyFloatDeviceData(port.alarmMax);
        this.reactionMax = port.reactionMax;
        this.delayMax.copyTimeDevice(port.delayMax);
    }

    public boolean compare(port port) {
        if (!(Arrays.equals(this.name, port.name)) ||
                (this.typeInput != port.typeInput) ||
                (this.viewSensor != port.viewSensor) ||
                (this.typeSensor != port.typeSensor) ||
                (this.normalState != port.normalState) ||
                (this.typeLevelSensor != port.typeLevelSensor) ||
                (this.measurementMin.data != port.measurementMin.data) ||
                (this.measurementMax.data != port.measurementMax.data) ||
                (this.rangeMin.data != port.rangeMin.data) ||
                (this.rangeMax.data != port.rangeMax.data) ||
                !(Arrays.equals(this.unit, port.unit)) ||
                (this.alarmMin.data != port.alarmMin.data) ||
                (this.reactionMin != port.reactionMin) ||
                !(this.delayMin.compare(port.delayMin)) ||
                (this.alarmMax.data != port.alarmMax.data) ||
                (this.reactionMax != port.reactionMax) ||
                !(this.delayMax.compare(port.delayMax)) ) return false;
            return true;
    }

    public byte[] getPort(byte number) {
        byte[] ret = new byte[46];
        ret[0] = number;
        System.arraycopy(this.name,0, ret, 1,15);
        ret[16] = this.typeInput;
        ret[17] = this.viewSensor;
        ret[18] = this.typeSensor;
        ret[19] = this.normalState;
        ret[20] = this.typeLevelSensor;
        ret[21] = this.measurementMin.fractData;
        ret[22] = this.measurementMin.integerData;
        ret[23] = this.measurementMax.fractData;
        ret[24] = this.measurementMax.integerData;
        ret[25] = this.rangeMin.fractData;
        ret[26] = this.rangeMin.integerData;
        ret[27] = this.rangeMax.fractData;
        ret[28] = this.rangeMax.integerData;
        System.arraycopy(this.unit,0, ret, 29,5);
        ret[34] = this.alarmMin.fractData;
        ret[35] = this.alarmMin.integerData;
        ret[36] = this.reactionMin;
        ret[37] = this.delayMin.h;
        ret[38] = this.delayMin.m;
        ret[39] = this.delayMin.s;
        ret[40] = this.alarmMax.fractData;
        ret[41] = this.alarmMax.integerData;
        ret[42] = this.reactionMax;
        ret[43] = this.delayMax.h;
        ret[44] = this.delayMax.m;
        ret[45] = this.delayMax.s;
        return  ret;
    }
}