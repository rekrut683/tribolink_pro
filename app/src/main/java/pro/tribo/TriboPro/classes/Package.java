package pro.tribo.TriboPro.classes;

import android.util.Log;

import java.util.Arrays;

public class Package {
    final private byte preambleH = (byte)0xAA;
    final private byte preambleL = (byte)0xBB;
    private short length = 0;
    private short currentLength = 0;
    public byte checkSum = 0;
    public byte chk = 0;
    private boolean isLongPacket;
    private boolean isOld;
    public byte[] dataLong;
    public byte[] dataOut;
    public byte[] dataOld;

    public Package() {
        this.isLongPacket = false;
        this.isOld = false;
    }
/* метод сборки пакета для отправки: на выходе массив - преамбула 2, длина 1, тело, контрольная сумма 1
    в "длину" входит только тело, в контрольную сумму - "длина" и "тело" */
    public byte[] packageBuilder(byte[] data) {
        this.dataOut = new byte[data.length+4];
        this.dataOut[0] = preambleH;
        this.dataOut[1] = preambleL;
        this.dataOut[2] = (byte) data.length;
        this.checkSum = (byte) data.length;
        for (byte i = 0; i<data.length; i++) {
            this.dataOut[i+3] = data[i];
            this.checkSum +=data[i];
        }
        this.dataOut[this.dataOut.length-1] = this.checkSum;

        return this.dataOut;
    }
/* метод сборки пакета на прием: на выходе массив без серводанных, пакет может собираться за несколько проходов */
    public boolean packageCollector(byte[] data) {
        if (!this.isLongPacket) {
            if (this.isOld) { // Есть остаточные данные
                this.dataLong = new byte[this.dataOld.length+data.length];
                System.arraycopy(this.dataOld,0,this.dataLong,0,this.dataOld.length);
                System.arraycopy(data,0,this.dataLong,this.dataOld.length,data.length);
                data = this.dataLong;
                this.isOld = false;
            }
            this.dataLong = new byte[(int)(0xFF & data[2])];
            if (data[0] == preambleH && data[1] == preambleL) {
                if (data[2] > data.length-3) {
                    // длинный пакет начало
                    this.currentLength = (byte) (data.length-3);
                    System.arraycopy(data,3,dataLong,0,data.length-3);
                    this.length = data[2];
                    this.isLongPacket = true;
                    return false;
                } else {
                    // короткий пакет
                    System.arraycopy(data,3,this.dataLong,0,dataLong.length);
                    return checkSumRight(this.dataLong, data[2], data[data.length - 1]);
                }
            } else return false;
        } else {
            // длинный пакет продолжение
            if ((this.currentLength + data.length) > this.length) {
                // реакция на данные длиннее пакета
              byte len = (byte) (this.length - this.currentLength);
                System.arraycopy(data, 0, this.dataLong, this.currentLength, len);
                    if((data.length-len-1) >0) {
                        this.dataOld = new byte[data.length-len-1];
                        for(byte i=0; i<this.dataOld.length; i++) {
                            this.dataOld[i] = data[i+len+1];
                        }
                        if (dataOld[0] == (byte) 0xAA) {
                            this.isOld = true;
                        }
                    }
                    // проверить контрольную сумму
                    if (checkSumRight(this.dataLong,(byte)this.dataLong.length,data[len])) {
                        this.isLongPacket = false;
                        return true;
                    } else {
                        this.isLongPacket = false;
                        return false;
                    }
            } else {
                // продолжаем собирать пакет
                System.arraycopy(data, 0, this.dataLong, this.currentLength, data.length);
                this.currentLength +=data.length;
                return false;
            }
        }
    }

    @Override
    public String toString() {
        return "Package{" +
                ", dataLong=" + Arrays.toString(dataLong) +
                ", dataOut=" + Arrays.toString(dataOut) +
                '}';
    }

    private boolean checkSumRight(byte[] data, byte sum, byte chkSum) {
        for (byte i=0; i<data.length; i++) {
            sum += data[i];
        }
        this.chk = sum;
        return sum == chkSum;
    }
}